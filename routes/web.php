<?php

use Illuminate\Support\Facades\Route;

Route::get('{path}', [App\Http\Controllers\WelcomeController::class, 'home'])
    ->where('path', '(.*)');

//Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'service'], function () {
//
//});
//
//Route::group(['middleware' => ['auth'], 'prefix' => 'service'], function () {
//
//});
//
//Route::get('/', 'WelcomeController@index');
//
//Route::group(['prefix' => 'ppdb'], function () {
//    Route::get('/register', 'Frontend\StudentRegisterController@register')->name('ppdb.register');
//    Route::get('/announcement', 'Frontend\StudentRegisterController@announcement')->name('ppdb.announcement');
//    Route::get('/print', 'Frontend\StudentRegisterController@print')->name('ppdb.print');
//
//    Route::get('print-pdf', 'Frontend\StudentRegisterController@export');
//    Route::get('/forget-id', 'Frontend\StudentRegisterController@fotgetId')->name('ppdb.forgetId');
//    Route::get('success-register/{nik}', 'Frontend\StudentRegisterController@successRegister')->name('ppdb.success');
//
////    Route::post('/register', 'Frontend\StudentRegisterController@store')->name('ppdb.register.post');
////    Route::post('/forget-id', 'Frontend\StudentRegisterController@storeForgetId')->name('ppdb.forgetId.post');
////    Route::post('announcement', 'Frontend\StudentRegisterController@studentAnnouncement');
//});
//
//Route::get('test-print', 'Frontend\StudentRegisterController@textcode');
//
//Route::get('/dashboard/', function () {
//    return redirect('dashboard/index');
//});
//
//Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {
//    Route::get('/index', 'DashboardController@index')->name('home');
//    Route::get('re-registration', "StudentHomeController@index")->name('student.reregistration');
//    Route::get('/{page}/{query}', 'HomeController@page')->name('dashboard.page');
//});
//
//Route::get('/home', function () {
//    return redirect('dashboard/index');
//});

//Route::get('/roles', 'PermissionController@Permission');
