<?php

use Illuminate\Support\Facades\Route;

Route::get('logo', 'WelcomeController@mainLogo');
Route::get('home-data', 'WelcomeController@homeData');
Route::get('welcome-data', 'WelcomeController@welcomeData');
Route::get('/expertise-by-major', 'Frontend\StudentRegisterController@expertiseByMajor');
Route::get('/all-major', 'Api\MajorController@allMajor');

Route::post("login", 'Api\AuthController@login');

Route::group(['middleware' => ['api', 'jwt.verify']], function () {
    Route::get('dashboard-data', "Api\HomeDataController@index");
    Route::get('role-user', 'Api\UserController@getRoles');
    Route::get('prospective-students-export', "Api\ProspectiveStudentController@exportData");
    Route::get('students', "Api\StudentController@index");
    Route::get('user', 'Api\AuthController@authUser');

    Route::get('re-registration', "Api\ReRegistrationController@index")->name('re_registration');
    Route::get('re-registration/{id}', "Api\ReRegistrationController@dataByDetails");
    Route::get('student-photo', "Api\StudentDetailsController@studentPhoto");
    Route::get('student-photo/{id}', "Api\StudentDetailsController@studentPhotoByUser");
    Route::get('download-photo/{id}', "Api\StudentDetailsController@downloadUserImage");
    Route::get('student-details', "Api\StudentDetailsController@details");
    Route::get('student-details/{id}', "Api\StudentDetailsController@detailsByUser");
    Route::get('student-data-help', "Api\StudentHelpDataController@index");
    Route::get('student-data-help/{id}', "Api\StudentHelpDataController@indexByUser");
    Route::get('student-data-parent', "Api\StudentParentController@index");
    Route::get('student-data-parent/{id}', "Api\StudentParentController@indexByUser");
    Route::get('student-score', "Api\StudentScoreController@score");
    Route::get('student-score/{id}', "Api\StudentScoreController@scoreByUser");
    Route::get('student-files', "Api\StudentFileController@index");

    Route::get('regency', "Api\PlaceController@regency");
    Route::get('districts', "Api\PlaceController@districts");
    Route::get('villages', "Api\PlaceController@villages");

    Route::post('student-photo', "Api\StudentDetailsController@storePhoto");
    Route::post('student-details', "Api\StudentDetailsController@store");
    Route::post('student-data-help', "Api\StudentHelpDataController@store");
    Route::post('student-data-parent', "Api\StudentParentController@store");
    Route::post('student-score', "Api\ReRegistrationController@index");
    Route::post('student-files', "Api\StudentFileController@store");

    Route::post('student-send-data', "Api\ReRegistrationController@sendData");

    Route::apiResource('student-registration', 'Api\StudentRegistrationController');
    Route::apiResource("prospective-students", "Api\ProspectiveStudentController");

    Route::group(["prefix" => "master"], function () {
        Route::get('home-data/details', "Api\HomeDataController@details");

        Route::apiResource('/major', 'Api\MajorController');
        Route::apiResource('/roles', 'Api\RoleController');
        Route::apiResource('/expertise', 'Api\ExpertiseController');
        Route::apiResource("/users", "Api\UserController");
        Route::apiResource("/menu", "Api\MasterMenuController");
        Route::apiResource("/extracurricular", "Api\ExtraCurricularController");
        Route::apiResource("/blood-group", "Api\BloodGroupController");
        Route::apiResource("/transportation", "Api\TransportationController");
        Route::apiResource("/resident", "Api\ResidentController");
        Route::apiResource("/religion", "Api\ReligionController");
        Route::apiResource("/special-needs", "Api\SpecialNeedsController");
        Route::apiResource("/province", "Api\ProvinceController");
        Route::apiResource("/regency", "Api\RegencyController");
        Route::apiResource("/district", "Api\DistrictController");
        Route::apiResource("/village", "Api\VillageController");
        Route::apiResource("/parent-job", "Api\ParentJobController");
        Route::apiResource("/school-status", "Api\SchoolStatusController");
        Route::apiResource("/pip-reason", "Api\WorthyReasonController");
        Route::apiResource("/income", "Api\IncomeController");
        Route::apiResource("/home-data", "Api\HomeDataController");
        Route::apiResource("/time-line", "Api\TimeLineController");
    });
});

Route::group(['prefix' => 'ppdb'], function () {
    Route::get('/print', 'Frontend\StudentRegisterController@export')->name('ppdb.print.registration');
    Route::post('/register', 'Frontend\StudentRegisterController@store')->name('ppdb.register.post');
    Route::post('/forget-id', 'Frontend\StudentRegisterController@storeForgetId')->name('ppdb.forgetId.post');
    Route::post('/announcement', 'Frontend\StudentRegisterController@studentAnnouncement');
});
