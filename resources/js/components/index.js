import Vue from 'vue'
import App from './layout/App'
import Auth from './layout/Auth'

Vue.component(App.name, App)
Vue.component(Auth.name, Auth)
