import Vue from "vue";

window.axios = require('axios');
window.axios.defaults.baseURL = document.head.querySelector('meta[name="api-base-url"]').content;

import store from './store'
import router from './router'
import App from './components/App'
import '../sass/app.scss'

import VueSweetalert2 from 'vue-sweetalert2';
// If you don't need the styles, do not connect
// import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);

// import plugin
import { TiptapVuetifyPlugin } from 'tiptap-vuetify'
// don't forget to import CSS styles
import 'tiptap-vuetify/dist/main.css'
import vuetify from './plugins/vuetify'
// use this package's plugin
Vue.use(TiptapVuetifyPlugin, {
  // the next line is important! You need to provide the Vuetify Object to this place.
  vuetify, // same as "vuetify: vuetify"
  // optional, default to 'md' (default vuetify icons before v2.0.0)
  iconsGroup: 'mdi'
})

require('./plugins/axios')

//Vue.prototype.$http = Axios;

import './components'

new Vue({
    vuetify,
    store,
    router,
    ...App
}).$mount('#app')
