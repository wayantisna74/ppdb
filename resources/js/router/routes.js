export default [
  {
    path: '/',
    name: 'index',
    meta: {layout: 'auth', requiresGuest: true},
    component: () => import('../pages/Welcome').then(m => m.default || m)
  },

  {
    path: '/login',
    name: 'login',
    meta: {layout: 'auth', requiresGuest: true},
    component: () => import('../pages/auth/Login').then(m => m.default || m)
  },

  {
    path: '/ppdb/register',
    name: 'Register',
    component: () => import('../pages/ppdb/Register').then(m => m.default || m),
    meta: {layout: 'auth', requiresGuest: true},
  },

  {
    path: '/ppdb/announcement',
    name: 'Announcement',
    component: () => import('../pages/ppdb/Announcement').then(m => m.default || m),
    meta: {layout: 'auth', requiresGuest: true},
  },

  {
    path: '/ppdb/print',
    name: 'PrintRegistration',
    component: () => import('../pages/ppdb/PrintRegistration').then(m => m.default || m),
    meta: {layout: 'auth', requiresGuest: true},
  },

  {
    path: '/welcome',
    name: 'welcome',
    component: () => import('../pages/dashboard/Dashboard').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'Dashboard'}
  },

  {
    path: '/home',
    name: 'home',
    component: () => import('../pages/dashboard/Dashboard').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'Dashboard'}
  },

  {
    path: '/dashboard/re-registration',
    name: 'ReRegistration',
    component: () => import('../pages/dashboard/ReRegistration').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'ReRegistration'}
  },

  {
    path: '/dashboard/prospective-student',
    name: 'ProspectiveStudent',
    component: () => import('../pages/dashboard/ProspectiveStudent').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'ReRegistration'}
  },

  {
    path: '/dashboard/master/ppdb',
    name: 'MasterPpdb',
    component: () => import('../pages/dashboard/master/Ppdb').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'Master PPPDB'}
  },

  {
    path: '/dashboard/master/major',
    name: 'MasterMajor',
    component: () => import('../pages/dashboard/master/Major').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'Master Jurusan'}
  },

  {
    path: '/dashboard/master/role',
    name: 'MasterRole',
    component: () => import('../pages/dashboard/master/Role').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'Master Peran'}
  },

  {
    path: '/dashboard/master/user',
    name: 'MasterUser',
    component: () => import('../pages/dashboard/master/User').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'Master User'}
  },
  {
    path: '/dashboard/master/home-data',
    name: 'MasterHomeData',
    component: () => import('../pages/dashboard/master/HomeData').then(m => m.default || m),
    meta: {requiresAuth: true, layout: 'default', title: 'Master Home Data'}
  },

  {path: '/404', meta: {layout: 'auth'}, component: () => import('../pages/errors/Error404').then(m => m.default || m)},
  {path: '*', meta: {layout: 'auth'}, component: () => import('../pages/errors/Error404').then(m => m.default || m)}
]
