<?php

return [
    /**
     * Welcome menu
     */
    'home' => 'Home',
    'roles' => 'Roles',
    'permissions' => 'Permissions',
    'school' => 'School',
    'alumni' => 'Alumni',
    'users' => 'Users',
    'students' => 'Students',
    'Majors' => 'Major',
    'add' => 'Add',
    'edit' => 'Edit',
    'update' => 'Update',
    'delete' => 'Delete',
    'save' => 'Save',
    'setting' => 'Setting',
    'Description' => 'Description',
    'Action' => 'Action',
    'Year' => 'Year',
    'Open Date' => 'Open Date',
    'Status' => 'Status',
    'student_registration' => 'Student Registration',
    'faq' => 'Frequently Asked Questions',
    'Re-registration' => 'Re-registration',
    'Expertise' => 'Expertise',
];
