<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'fullName' => 'Full Name',
    'palacebirtday' => 'Place',
    'dob' => 'Date Of Birthday',
    'nisn' => 'NISN',
    'originSchool' => 'Origin Junior High School',
    'nik' => 'NIK',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'address' => 'Address',
    'reportCard' => 'Report Card',
    'average' => 'Average',
    'personalInformation' => 'Personal Information',
    'indonesian' => 'Indonesian',
    'english' => 'English',
    'math' => 'Math',
    'major' => 'Major',
    'expertise' => 'Expertise',
    'phoneNumber' => 'Phone Number',
    'haveKIP' => 'Have KIP?',
    'parentData' => 'Parent or Guardian Data',
    'parent' => 'Parents',
    'student' => 'Prospective New Students',
    'father' => 'Father',
    'mother' => 'Mother',
    'yes' => 'Yes',
    'no' => 'No',
    'submit' => 'Register',
    'tosStatement' => 'Fill in the statement below with a check mark',
    'longTos' => 'I am willing to complete the file if it is declared PASS PPDB Selection and
            ready declared NOT PASS if it does not complete the file on time',

];
