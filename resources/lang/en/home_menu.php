<?php

return [
    /**
     * Welcome menu
     */
    'home' => 'Home',
    'register' => 'Register',
    'announcement' => 'Announcement',
    'print' => 'Print',
    'alumni' => 'Alumni',
    'forgetId' => 'Forget Registration Code',
];
