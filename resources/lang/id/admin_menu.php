<?php

return [
    /**
     * Welcome menu
     */
    'home' => 'Beranda',
    'roles' => 'Peran',
    'permissions' => 'Hak Akses',
    'school' => 'Sekolah',
    'alumni' => 'Alumni',
    'users' => 'Pengguna',
    'students' => 'Data Siswa',
    'prospective_student' => 'Data Calon Siswa',
    'Majors' => 'Jurusan',
    'add' => 'Tambah',
    'edit' => 'Ubah',
    'view' => 'Lihat',
    'update' => 'Ubah',
    'delete' => 'Hapus',
    'approve' => 'Terima',
    'reject' => 'Tolak',
    'refresh' => 'Segarkan',
    're-regis' => 'Daftar Ulang',
    'save' => 'Simpan',
    'setting' => 'Pengaturan',
    'Description' => 'Penjelasan',
    'Action' => 'Aksi',
    'Year' => 'Tahun',
    'Open Date' => 'Tanggal dibuka',
    'Status' => 'Status',
    'Open' => 'Buka',
    'Close' => 'Tutup',
    'student_registration' => 'PPDB',
    'faq' => 'pertanyaan yang sering diajukan',
    'Re-registration' => 'Pendaftaran Ulang',
    'Expertise' => 'Bidang Keahlian',
    'Export Excel' => 'Export Excel',
    'pending' => 'Tunda',
    'Registration close' => 'Pendaftaran telah ditutup!',
    'Registration Date' => 'Waktu Pendaftaran',
];
