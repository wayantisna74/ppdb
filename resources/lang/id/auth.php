<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Kredensial ini tidak cocok dengan catatan kami.',
    'throttle' => 'Terlalu banyak upaya masuk. Silakan coba lagi dalam: :seconds detik.',
    'fullName' => 'Nama Lengkap',
    'registrationCode' => 'Kode Pendaftaran',
    'palacedob' => 'Tempat/Tanggal Lahir',
    'Print' => 'Cetak',
    'Date Register' => 'Tanggal Daftar',
    'palacebirtday' => 'Tempat Lahir',
    'dob' => 'Tanggal Lahir',
    'nisn' => 'NISN',
    'originSchool' => 'Sekolah SMP Asal',
    'nik' => 'NIK',
    'gender' => 'Jenis Kelamin',
    'male' => 'Laki-laki',
    'female' => 'Perempuan',
    'address' => 'Alamat',
    'reportCard' => 'Nilai Raport',
    'average' => 'Rata-rata',
    'indonesian' => 'Bahasa Indonesia',
    'english' => 'Bahasa Inggris',
    'math' => 'Matematika',
    'major' => 'Jurusan',
    'expertise' => 'Program Bid. Keahlian',
    'phoneNumber' => 'Nomor HP',
    'haveKIP' => 'Memiliki Kartu KIP',
    'parentData' => 'Data Orang Tua atau Wali',
    'parent' => 'Orang Tua',
    'student' => 'Calon Peserta Didik Baru',
    'father' => 'Ayah',
    'mother' => 'Ibu',
    'yes' => 'Ya',
    'no' => 'Tidak',
    'submit' => 'Daftar',
    'tosStatement' => 'Isilah pernyataan dibawah ini dengan tanda centang (v)',
    'longTos' => 'Saya bersedia melengkapi berkas jika dinyatakan LULUS Seleksi PPDB dan
        siap dinyatakan TIDAK LULUS jika tidak melengkapi berkas tepat pada waktunya',

];
