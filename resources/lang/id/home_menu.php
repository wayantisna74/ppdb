<?php

return [
    /**
     * Welcome menu
     */
    'home' => 'Beranda',
    'register' => 'Daftar',
    'announcement' => 'Pengumuman',
    'print' => 'Cetak',
    'alumni' => 'Alumni',
    'forgetId' => 'Lupa Code Registrasi',
];
