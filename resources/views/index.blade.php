<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
  <meta http-equiv="Pragma" content="no-cache"/>
  <meta http-equiv="Expires" content="0"/>

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="manifest" href="{{ asset('manifest.json') }}">
  {{--    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">--}}

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="Add to Home">
  <meta name="android-mobile-web-app-title" content="Add to Home">
  <meta name="theme-color" content="#2196F3">
  <meta name="api-base-url" content="{{ url('/') }}"/>
  <meta name="route-base-url" content="{{ env('ROUTE_BASE') }}"/>
  <link rel="icon" type="image/png" href="{{ asset('images/icons/icon-152x152.png') }}"/>

  <title>{{ config('app.name') }}</title>
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">--}}
  <link rel="stylesheet" href="{{ mix('css/style.css') }}">
  <script>
    window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
                'siteName'  => config('app.name'),
                'apiDomain' => env('APP_URL').'/api',
            ]) !!}
  </script>
</head>
<body>
<noscript>
  This application needs JavaScript to work, please enable JavaScript on your browser
</noscript>
<div id="app"></div>

@php
  $config = [
      'appName' => config('app.name'),
      'locale' => $locale = app()->getLocale()
  ];
@endphp
<script>window.config = {!! json_encode($config); !!};</script>

{{-- Load the application scripts --}}


{{--<script>--}}
{{--  // Check that service workers are supported--}}
{{--  if ('serviceWorker' in navigator) {--}}
{{--    // Use the window load event to keep the page load performant--}}
{{--    window.addEventListener('load', () => {--}}
{{--      navigator.serviceWorker.register('/service-worker.js');--}}
{{--    });--}}
{{--  }--}}
{{--</script>--}}

<script src="{{ asset(mix('assets//js/manifest.js')) }}"></script>
<script src="{{ asset(mix('assets/vendor1.js')) }}"></script>
<script src="{{ asset(mix('assets/vendor2.js')) }}"></script>
<script src="{{ asset(mix('assets//js/app.js')) }}"></script>

</body>
</html>
