@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12 mb-3 mt-3">
                    <div class="hero bg-primary text-white">
                        <div class="hero-inner">
                            <h2>Selamat Datang</h2>
                            <p class="lead">Selamat datang di SISTEM Informasi online {{ env('APP_NAME') }}</p>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="col-md-4">
                    <h2 class="section-title">{{ __('admin_menu.Registration Date') }}</h2>
                    <div class="activities">
                        @foreach($timeline as $data)
                            <div class="activity">
                                <div class="activity-icon bg-primary text-white shadow-primary">
                                    <i class="{{ $data['icon'] }}"></i>
                                </div>
                                <div class="activity-detail">
                                    <div class="mb-2">
                                        <span class="text-job text-primary">{{ $data['date'] }}</span>
                                        <span class="bullet"></span>
                                    </div>
                                    <p>{{ $data['text'] }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="section-title">Jumlah Siswa Yang diterima</h2>
                            <ul>
                                <li>Teknik Produksi Migas : 72 siswa</li>
                                <li>Teknik Pengolahan Migas dan Petrokimia : 72 siswa</li>
                            </ul>

                            <h2 class="section-title">Persyaratan yang harus disiapkan oleh Calon Peserta Didik Baru Tahun 2020/2021</h2>
                            <ul>
                                <li>Surat Keterangan Lulus dari sekolah, atau Ijazah bagi lulusan tahun 2019 dan 2018</li>
                                <li>FC. Raport yang dilegalisir cap basah 1 (satu) rangkap</li>
                                <li>FC. Surat kelakuan baik dari sekolah 1 (satu) rangkap</li>
                                <li>FC. Kartu keluarga 1 (satu) rangkap</li>
                                <li>FC. Akta Kelahiran 1 (satu) rangkap</li>
                                <li>FC. Kartu PIP / KIP ( bagi yang memiliki ) 1 (satu) rangkap</li>
                                <li>Mengisi Surat Pernyataan Tata Tertib Sekolah dan materai 6000</li>
                                <li>Pas photo ukuran 3x 4 cm berupa file JPG berlatar belakang merah.</li>
                            </ul>

                            <h2 class="section-title">
                                Jika pendaftar calon Siswa baru melebihi kuota yang diterima maka akan diadakan seleksi berupa ujian tertulis mata pelajaran : Bahasa Indonesia, Bahasa Inggris, Matematika dan IPA.
                            </h2>

                            <h2 class="section-title">
                                Bagi calon peserta didik baru yang dinyatakan LULUS dan tidak melakukan registrasi secara online sampai tanggal 29 juni 2020 pukul 12.00 WITA , maka dianggap mengundurkan diri sebagai calon peserta baru pada SMKN 2 Toili Barat.
                            </h2>
                            <h2 class="section-title">
                                Pada saat pelaksanaan Pengenalan lingkungan sekolah tanggal 8 s/10 juli 2020 Peserta Didik Baru membawa dan dikumpul persyaratan pada point 3 (tiga) untuk :
                            </h2>
                            <ul>
                                <li>Prog. keahlian Teknik Produksi Migas menggunakan map snelhekter warna Biru, dan</li>
                                <li>Prog. keahlian Teknik Pengolahan Migas & petrokimia menggunakan map snelhekter warna Merah</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
