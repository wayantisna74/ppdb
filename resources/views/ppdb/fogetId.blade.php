@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-body">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Cek Hasil Pengumuman Seleksi PPDB</h4>
                        </div>
                        <div class="card-body">
                            @if (session('student'))
                                <div
                                    class="alert @if(session('status') == 're-registration') alert-success @else alert-danger @endif  alert-has-icon">
                                    <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                                    <div class="alert-body">
                                        <div class="alert-title">Nomor Registrasi</div>
                                        Nomor re
                                    </div>
                                </div>
                            @endif
                            <form method="POST" action="{{ route('ppdb.forgetId.post') }}">
                                @csrf
                                <div class="form-group row mb-2">
                                    <label for="name"
                                           class="col-md-3 col-form-label ">{{ __('auth.nisn') }}</label>
                                    <div class="col-md-8">
                                        <input id="nisn" name="nisn" value="{{ old('nisn') }}" required type="number"
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row mb-2">
                                    <label for="name"
                                           class="col-md-3 col-form-label ">{{ __('Password') }}</label>
                                    <div class="col-md-8">
                                        <input id="nisn" name="passowrd" required type="password" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-12 text-left">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Kirim') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
