@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-body">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @if($checkOpen)
                        @if($checkOpen->is_open == 'Y')
                            @if (session('success-register'))
                                <div class="alert alert-success alert-has-icon">
                                    <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                                    <div class="alert-body">
                                        <div class="alert-title">Success</div>
                                        {{ session('success-register') }}
                                    </div>
                                </div>
                            @endif

                            <form action="{{ route('ppdb.register.post') }}" method="POST">
                                @csrf
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Daftar PPDB</h4>
                                    </div>
                                    <div class="card-body">
                                        <span class="section-title mt-0">Data Pribadi</span>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="inputEmail4">{{ __('auth.fullName') }}</label>
                                                <input type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name" value="{{ old('name') }}" required autofocus>

                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4 col-6">
                                                <label for="inputEmail4">{{ __('auth.palacebirtday') }}</label>
                                                <input type="text"
                                                       class="form-control @error('born_place') is-invalid @enderror"
                                                       name="born_place" value="{{ old('born_place') }}" required>

                                                @error('born_place')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4 col-6">
                                                <label for="inputEmail4">{{ __('auth.dob') }}</label>
                                                <input type="text"
                                                       class="form-control flatpickr2 @error('dob') is-invalid @enderror"
                                                       name="dob"
                                                       value="{{ old('dob') }}" required>

                                                @error('dob')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="inputEmail4">{{ __('auth.nisn') }}</label>
                                                <input type="number" minlength="10" maxlength="10"
                                                       class="form-control @error('no_nisn') is-invalid @enderror"
                                                       name="no_nisn" value="{{ old('no_nisn') }}" required autofocus>

                                                @error('no_nisn')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label for="inputEmail4">{{ __('auth.originSchool') }}</label>
                                                <input type="text"
                                                       class="form-control @error('old_school') is-invalid @enderror"
                                                       name="old_school" value="{{ old('old_school') }}" required>

                                                @error('old_school')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label for="inputEmail4">{{ __('auth.nik') }}</label>
                                                <input type="number" minlength="16" maxlength="16"
                                                       class="form-control @error('no_nik') is-invalid @enderror"
                                                       name="no_nik" value="{{ old('no_nik') }}" required>

                                                @error('no_nik')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="inputEmail4">{{ __('auth.gender') }}</label>
                                                <select class="form-control @error('gender') is-invalid @enderror"
                                                        name="gender"
                                                        id="gender">
                                                    <option
                                                        value="{{ __('auth.female') }}">{{ __('auth.female') }}</option>
                                                    <option value="{{ __('auth.male') }}">{{ __('auth.male') }}</option>
                                                </select>

                                                @error('gender')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-8">
                                                <label for="inputEmail4">{{ __('auth.address') }}</label>
                                                <input type="text"
                                                       class="form-control @error('address') is-invalid @enderror"
                                                       name="address" value="{{ old('address') }}" required>

                                                @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="inputEmail4">{{ __('auth.haveKIP') }}</label>
                                                <select class="form-control @error('hasKIP') is-invalid @enderror"
                                                        name="hasKIP"
                                                        id="hasKIP">
                                                    <option
                                                        value="Y">Ya
                                                    </option>
                                                    <option value="N">Tidak</option>
                                                </select>

                                                @error('hasKIP')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-8">
                                                <label for="inputEmail4">{{ __('auth.phoneNumber') }}</label>
                                                <input type="number" minlength="12" maxlength="13"
                                                       class="form-control @error('no_phone') is-invalid @enderror"
                                                       name="no_phone" value="{{ old('no_phone') }}" required>

                                                @error('no_phone')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <span class="section-title mt-0">{{ __('auth.reportCard') }}</span>
                                        <small>Untuk menulis decimal dapat menggunakan tanda titik misalkan
                                            80.50</small>
                                        <hr>
                                        <div class="form-row">
                                            <span class="col-12">Bahasa Indonesia</span>
                                            <input type="hidden" value="Bahasa Indonesia" name="report[lesson][]">
                                            @for($i=1; $i <= 5; $i++)
                                                <div class="form-group col-4 col-sm-2 text-center">
                                                    <label for="inputEmail4" class="">{{ $i }}</label>
                                                    <input onchange="calcAverage('bindo_semester_', 'average_bindo')"
                                                           id="{{ 'bindo_semester_'.$i }}" type="text"
                                                           class="formatNumber form-control number_bindo_{{ $i }} @error('bindo_semester_'.$i) is-invalid @enderror"
                                                           name="{{ 'bindo_semester_'.$i }}"
                                                           value="{{ old('bindo_semester_'.$i) }}" required
                                                           autofocus>

                                                    @error('bindo_semester_'.$i)
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                    @enderror
                                                </div>
                                            @endfor

                                            <div class="form-group col-4 col-sm-2">
                                                <label for="inputEmail4">{{ __('auth.average') }}</label>
                                                <input type="text" id="average_bindo"
                                                       readonly
                                                       class="formatNumber form-control average_1 @error('average_bindo') is-invalid @enderror"
                                                       name="average_bindo" value="{{ old('average_bindo') }}" required>

                                                @error('average_bindo')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <span class="col-12">Bahasa Inggris</span>
                                            <input type="hidden" value="Bahasa Inggris" name="report[lesson][]">
                                            @for($i=1; $i <= 5; $i++)
                                                <div class="form-group col-4 col-sm-2 text-center">
                                                    <label for="inputEmail4" class="text-center">{{ $i }}</label>
                                                    <input onchange="calcAverage('bing_semester_', 'average_binng')"
                                                           id="{{ 'bing_semester_'.$i }}" type="text"
                                                           class="formatNumber form-control number_bing_{{ $i }} @error('bing_semester_'.$i) is-invalid @enderror"
                                                           name="{{ 'bing_semester_'.$i }}"
                                                           value="{{ old('bing_semester_'.$i) }}" required
                                                           autofocus>

                                                    @error('bing_semester_'.$i)
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                    @enderror
                                                </div>
                                            @endfor

                                            <div class="form-group col-4 col-sm-2">
                                                <label for="inputEmail4">{{ __('auth.average') }}</label>
                                                <input type="text" id="average_binng"
                                                       readonly
                                                       class="formatNumber form-control average_2 @error('average_binng') is-invalid @enderror"
                                                       name="average_binng" value="{{ old('average_binng') }}" required>

                                                @error('average_binng')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <span class="col-12">Matematika</span>
                                            <input type="hidden" value="Matematika" name="report[lesson][]">
                                            @for($i=1; $i <= 5; $i++)
                                                <div class="form-group col-4 col-sm-2 text-center">
                                                    <label for="inputEmail4" class="text-center">{{ $i }}</label>
                                                    <input onchange="calcAverage('mtk_semester_', 'average_mtk')"
                                                           id="{{ 'mtk_semester_'.$i }}" type="text"
                                                           class="formatNumber form-control number_math_{{ $i }} @error('mtk_semester_'.$i) is-invalid @enderror"
                                                           name="{{ 'mtk_semester_'.$i }}"
                                                           value="{{ old('mtk_semester_'.$i) }}" required
                                                           autofocus>

                                                    @error('mtk_semester_'.$i)
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                    @enderror
                                                </div>
                                            @endfor

                                            <div class="form-group col-4 col-sm-2">
                                                <label for="inputEmail4">{{ __('auth.average') }}</label>
                                                <input type="text" id="average_mtk"
                                                       readonly
                                                       class="formatNumber form-control average_3 @error('average_mtk') is-invalid @enderror"
                                                       name="average_mtk" value="{{ old('average_mtk') }}" required>

                                                @error('average_mtk')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <span class="col-12">IPA</span>
                                            <input type="hidden" value="IPA" name="report[lesson][]">
                                            @for($i=1; $i <= 5; $i++)
                                                <div class="form-group col-4 col-sm-2 text-center">
                                                    <label for="inputEmail4" class="text-center ">{{ $i }}</label>
                                                    <input onchange="calcAverage('ipa_semester_', 'average_ipa')"
                                                           id="{{ 'ipa_semester_'.$i }}" type="text"
                                                           class="formatNumber form-control number_ipa_{{ $i }} @error('ipa_semester_'.$i) is-invalid @enderror"
                                                           name="{{ 'ipa_semester_'.$i }}"
                                                           value="{{ old('ipa_semester_'.$i) }}"
                                                           required
                                                           autofocus>

                                                    @error('ipa_semester_'.$i)
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                    @enderror
                                                </div>
                                            @endfor

                                            <div class="form-group col-4 col-sm-2">
                                                <label for="inputEmail4">{{ __('auth.average') }}</label>
                                                <input type="text" id="average_ipa"
                                                       readonly
                                                       class="formatNumber form-control average_4 @error('average_ipa') is-invalid @enderror"
                                                       name="average_ipa" value="{{ old('average_ipa') }}" required>

                                                @error('average_ipa')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <span class="section-title mt-0">{{ __('admin_menu.Majors') }}</span>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">{{ __('admin_menu.Majors') }}</label>
                                                <select class="form-control @error('major_id') is-invalid @enderror"
                                                        name="major_id"
                                                        id="major_id">
                                                    @foreach($majors as $major)
                                                        <option value="{{ $major->id }}">{{ $major->name }}</option>
                                                    @endforeach
                                                </select>

                                                @error('major_id')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">{{ __('admin_menu.Expertise') }}</label>
                                                <select class="form-control @error('expertise_id') is-invalid @enderror"
                                                        name="expertise_id" required
                                                        id="expertise_id">
                                                </select>

                                                @error('expertise_id')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <span class="section-title mt-0">{{ __('auth.parentData') }}</span>
                                        <hr>
                                        <div class="form-row">
                                            <label class="col-md-12">{{ __('auth.father') }}</label>
                                            <div class="form-group col-md-4">
                                                <label for="inputEmail4">{{ __('auth.fullName') }}</label>
                                                <input type="text"
                                                       class="form-control @error('name_father') is-invalid @enderror"
                                                       name="name_father" value="{{ old('name_father') }}" required
                                                       autofocus>

                                                @error('name_father')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4 col-6">
                                                <label for="inputEmail4">{{ __('auth.nik') }}</label>
                                                <input type="text"
                                                       class="form-control @error('nik_father') is-invalid @enderror"
                                                       name="nik_father" value="{{ old('nik_father') }}" required>

                                                @error('nik_father')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4 col-6">
                                                <label for="inputEmail4">{{ __('auth.address') }}</label>
                                                <input type="text"
                                                       class="form-control @error('address_father') is-invalid @enderror"
                                                       name="address_father"
                                                       value="{{ old('address_father') }}" required>

                                                @error('address_father')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <label class="col-md-12">{{ __('auth.mother') }}</label>
                                            <div class="form-group col-md-4">
                                                <label for="inputEmail4">{{ __('auth.fullName') }}</label>
                                                <input type="text"
                                                       class="form-control @error('name_mother') is-invalid @enderror"
                                                       name="name_mother" value="{{ old('name_mother') }}" required
                                                       autofocus>

                                                @error('name_mother')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4 col-6">
                                                <label for="inputEmail4">{{ __('auth.nik') }}</label>
                                                <input type="text"
                                                       class="form-control @error('nik_mother') is-invalid @enderror"
                                                       name="nik_mother" value="{{ old('nik_mother') }}" required>

                                                @error('nik_mother')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>

                                            <div class="form-group col-md-4 col-6">
                                                <label for="inputEmail4">{{ __('auth.address') }}</label>
                                                <input type="text"
                                                       class="form-control @error('address_mother') is-invalid @enderror"
                                                       name="address_mother"
                                                       value="{{ old('address_mother') }}" required>

                                                @error('address_mother')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <span class="section-title mt-0">Pernyataan</span>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <div
                                                    class="custom-control custom-checkbox @error('address_mother') is-invalid @enderror">
                                                    <input type="checkbox"
                                                           value="Y" {{ old('agreeTos') == 'Y' ? 'checked' : '' }}
                                                           name="agreeTos" required class="custom-control-input"
                                                           id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">
                                                        Saya bersedia melengkapi berkas jika dinyatakan LULUS Seleksi
                                                        PPDB
                                                        dan
                                                        siap dinyatakan TIDAK LULUS jika tidak melengkapi berkas tepat
                                                        pada
                                                        waktunya
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div
                                                    class="custom-control @error('address_mother') is-invalid @enderror custom-checkbox">
                                                    <input type="checkbox"
                                                           value="Y" {{ old('parentCheck') == 'Y' ? 'checked' : '' }}
                                                           name="parentCheck" required class="custom-control-input"
                                                           id="customCheck2">
                                                    <label class="custom-control-label" for="customCheck2">
                                                        Telah mendapatkan izin dari orang tua untuk melakukan
                                                        pendaftaran
                                                        di {{ env('APP_NAME') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-primary">{{ __('auth.submit') }}</button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    @else
                        <div class="alert alert-warning alert-has-icon">
                            <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                            <div class="alert-body">
                                <div class="alert-title">Maaf</div>
                                {{ __('Pendaftaran Peserta Didik Baru Belum dimulai, Silahkan mengungjungi website secara rutin untuk mendapatkan info selangkapnya') }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(function () {
            $('.flatpickr2').flatpickr({
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d",
                defaultDate: "@php echo(date('Y')-16).'-12-31' @endphp",
                minDate: "@php echo(date('Y')-21).'-01' @endphp"
            });

            for (let i = 1; i <= 5; i++) {

                if (document.getElementsByClassName('number_math_' + i).length > 0) {
                    new Cleave('.number_math_' + i, {
                        numeral: true,
                        numeralDecimalMark: ',',
                        delimiter: '.'
                    })
                }

            }

            var selectVal = $("#major_id option:selected").val();
            changeSelect(selectVal);
            // When an option is changed, search the above for matching choices
            $('#major_id').on('change', function () {
                // Set selected option as variable
                var selectValue = $(this).val();
                $('#expertise_id').children().remove();
                changeSelect(selectValue);
            });
        });

        function changeSelect(selectVal) {
            $.ajax({
                url: '{{ url('/service/expertise-by-major') }}',
                data: {
                    id: selectVal
                },
                method: 'GET',
                dataType: 'JSON',
                success: function (result) {
                    if (result) {
                        $.each(result, function (key, value) {
                            $('#expertise_id')
                                .append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.name));
                        })
                    }
                }
            })
        }

        function calcAverage(params, averageInput) {
            var total = 0;
            var arr_semester = [];
            for (var i = 1; i <= 5; i++) {
                var semester = $('#' + params + i).val().replace(',', '');
                var semesterVal
                if (semester && !isNaN(semester)) {
                    semesterVal = parseFloat(semester)
                } else {
                    semesterVal = 0
                }
                arr_semester.push(semesterVal)
                //console.log(isNaN(semester))
                total += parseFloat(semesterVal);
            }
            var average = parseFloat(total) / 5;
            $('#' + averageInput).val(average)
        }

    </script>
@endsection
