@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-body">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        @if($student)
                            <div class="card-header">
                                <h4>Cetak Bukti Pendaftaran</h4>
                            </div>
                            <div class="card-body">
                                <form method="Get" action="{{ url('ppdb/print-pdf') }}">
                                    @csrf
                                    <div class="form-group row mb-2">
                                        <label for="name" class="col-md-3 col-form-label ">{{ __('auth.registrationCode') }}</label>
                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control form-control-sm" value="{{ $student->ppdb_code }}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <label for="name" class="col-md-3 col-form-label ">{{ __('auth.nisn') }}</label>
                                        <div class="col-md-8">
                                            <input id="name" type="text" name="nisn" class="form-control form-control-sm" value="{{ $student->no_nisn }}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <label for="name" class="col-md-3 col-form-label ">{{ __('auth.fullName') }}</label>
                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control form-control-sm" value="{{ $student->name }}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <label for="name" class="col-md-3 col-form-label ">{{ __('auth.palacedob') }}</label>
                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control form-control-sm" value="{{ $student->born_place.' / '.$student->dob }}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <label for="name" class="col-md-3 col-form-label ">{{ __('admin_menu.Majors') }}</label>
                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control form-control-sm" value="{{ $student->major->name .'/'.$student->expertise->name }}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-2">
                                        <label for="name" class="col-md-3 col-form-label ">{{ __('auth.Date Register') }}</label>
                                        <div class="col-md-8">
                                            <input id="name" type="text" class="form-control form-control-sm" value="{{ $student->created_at }}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-12 text-left">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('auth.Print') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @else
                            <div class="alert alert-success alert-has-icon">
                                <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                                <div class="alert-body">
                                    <div class="alert-title">Registrasi tidak ditemukan</div>
                                    Tidak ada registrasi yang sesuai dengan query
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
