<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/stisla.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('imgs/logo.png') }}" type="image/png" sizes="16x16">
    <script>
        window.app = '{{ env('APP_URL').'/api' }}' ;
    </script>
    <style>
        body.layout-3 .main-content {
            padding-left: 0;
            padding-right: 0;
            padding-top: 16px;
        }

        body.layout-3 .navbar.navbar-secondary {
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.03);
            background-color: #fff;
            top: 0;
            padding: 0;
            z-index: 889;
        }

        a.nav-link.sidebar-gone-show {
            padding-left: 28px;
        }

        .fa-2x {
            font-size: 2em;
        }

        @media (max-width: 1024px) {
            .main-content {
                padding-top: 0;
            }
        }
    </style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>
<body
    class="layout-3"
>
<div id="app">

    <div class="main-wrapper container">
        <div class="navbar-nav mb-3 bg-white">
            <div class="d-inline-flex flex-row">
                <a href="#" class="nav-link sidebar-gone-show mt-2" data-toggle="sidebar">
                    <i class="fas fa-2x fa-bars"></i>
                </a>
                <a style="margin: 0 auto;" href="{{ url('/') }}" class="navbar-brand">
                    <img class="img" src="{{ asset('imgs/web.png') }}" alt="{{ env('APP_NAME') }}">
                </a>
            </div>
        </div>
        <nav class="navbar navbar-secondary navbar-expand-lg">
            <div class="container">
                <a href="{{ url('/') }}" class="navbar-brand sidebar-gone-hide">
                    <img class="img" src="{{ asset('imgs/web.png') }}" alt="{{ env('APP_NAME') }}">
                </a>

                <ul class="navbar-nav">
                    <li class="nav-item {{ (request()->is('/')) ? 'active' : '' }}">
                        <a href="{{ url('/') }}" class="nav-link">
                            <i class="fas fa-home"></i><span>{{ __('home_menu.home') }}</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" data-toggle="dropdown" class="nav-link has-dropdown">
                            <i class="fas fa-fire"></i><span>PPDB</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="nav-item {{ (request()->is('ppdb/register')) ? 'active' : '' }}">
                                <a href="{{ route('ppdb.register') }}" class="nav-link">
                                    <span>{{ __('home_menu.register') }}</span>
                                </a>
                            </li>
                            <li class="nav-item {{ (request()->is('ppdb/announcement')) ? 'active' : '' }}">
                                <a href="{{ route('ppdb.announcement') }}" class="nav-link">
                                    <span>{{ __('home_menu.announcement') }}</span>
                                </a>
                            </li>
                            <li class="nav-item {{ (request()->is('ppdb/print')) ? 'active' : '' }}">
                                <a href="{{ route('ppdb.print') }}" class="nav-link"><span>{{ __('home_menu.print') }}</span></a>
                            </li>
{{--                            <li class="nav-item {{ (request()->is('ppdb/forget-id')) ? 'active' : '' }}">--}}
{{--                                <a href="{{ route('ppdb.forgetId') }}" class="nav-link"><span>{{ __('home_menu.forgetId') }}</span></a>--}}
{{--                            </li>--}}
                        </ul>
                    </li>
                    <li class="nav-item {{ (request()->is('/alumni')) ? 'active' : '' }}">
                        <a href="{{ url('/alumni') }}" class="nav-link">
                            <i class="fas fa-people-arrows"></i><span>{{ __('home_menu.alumni') }}</span>
                        </a>
                    </li>
                    <li class="nav-item {{ (request()->is('/login')) ? 'active' : '' }}">
                        <a href="{{ url('/login') }}" class="nav-link">
                            <i class="fas fa-arrow-alt-circle-right"></i><span>{{ __('Login') }}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <main class="main-content">
            @yield('content')
        </main>
        <footer class="main-footer">
            <div class="footer-left">
                Copyright &copy; {{ date('Y') }} {{ env('APP_NAME') }}
                <div class="bullet"></div>
                Develop By <a target="_blank" href="https://codingofcents.com">Tisna Adi</a>
            </div>
            <div class="footer-right">
                2.3.0
            </div>
        </footer>
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/manifest.js') }}"></script>
<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@yield('script')
</body>
</html>
