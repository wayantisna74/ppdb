<div class="navbar-inner">
    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Nav items -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link {{ (request()->is('dashboard')) ? 'active' : '' }}"
                   href="{{ route('home') }}">
                    <i class="ni ni-spaceship"></i>
                    <span class="nav-link-text">{{ __('admin_menu.home') }}</span>
                </a>
            </li>
            @if(auth()->user()->role->name == 'Admin' || auth()->user()->role->name == 'Superuser')
                <li class="nav-item">
                    <a class="nav-link {{ (request()->is('dashboard/registration/prospective-students')) ? 'active' : '' }}"
                       href="#navbar-prospec-student" data-toggle="collapse" role="button"
                       aria-expanded="true" aria-controls="navbar-prospec-student">
                        <i class="ni ni-shop text-primary"></i>
                        <span class="nav-link-text">{{ __('admin_menu.student_registration') }}</span>
                    </a>
                    <div
                        class="collapse {{ (request()->is('dashboard/registration/prospective-students')) ? 'show' : '' }}"
                        id="navbar-prospec-student">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="{{ route('dashboard.page',['registration', 'prospective-students']) }}"
                                   class="nav-link">
                                    <span
                                        class="sidenav-mini-icon"> {{ substr(__('admin_menu.students'), 0, 1) }} </span>
                                    <span class="sidenav-normal"> {{ __('admin_menu.students') }} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link {{ (request()->is('dashboard/page/alumni')) ? 'active' : '' }}"--}}
{{--                       href="{{ route('dashboard.page',['page', 'alumni']) }}">--}}
{{--                        <i class="far fa-square"></i>--}}
{{--                        <span class="nav-link-text">{{ __('admin_menu.alumni') }}</span>--}}
{{--                    </a>--}}
{{--                </li>--}}

                <li class="nav-item">
                    <a class="nav-link {{ (request()->is('dashboard/page/students')) ? 'active' : '' }}"
                       href="{{ route('dashboard.page',['page', 'students']) }}">
                        <i class="ni ni-ungroup text-orange"></i>
                        <span class="nav-link-text">{{ __('admin_menu.students') }}</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link {{ (request()->is('dashboard/settings/*')) ? 'active' : '' }}"
                       href="#navbar-settings" data-toggle="collapse" role="button"
                       aria-expanded="true" aria-controls="navbar-settings">
                        <i class="fas fa-cog"></i>
                        <span class="nav-link-text">{{ __('admin_menu.setting') }}</span>
                    </a>
                    <div class="collapse {{ (request()->is('dashboard/settings/*')) ? 'show' : '' }}" id="navbar-settings">
                        <ul class="nav nav-sm flex-column">
{{--                            <li class="nav-item">--}}
{{--                                <a href="{{ route('dashboard.page', ['settings', 'school']) }}"--}}
{{--                                   class="nav-link {{ (request()->is('dashboard/page/school')) ? 'active' : '' }}">--}}
{{--                                    <span--}}
{{--                                        class="sidenav-mini-icon"> {{ substr(__('admin_menu.school'), 0, 1) }} </span>--}}
{{--                                    <span class="sidenav-normal"> {{ __('admin_menu.school') }} </span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
                            <li class="nav-item">
                                <a href="{{ route('dashboard.page', ['settings', 'majors']) }}"
                                   class="nav-link {{ (request()->is('dashboard/page/majors')) ? 'active' : '' }}">
                                    <span
                                        class="sidenav-mini-icon"> {{ substr(__('admin_menu.Majors'), 0, 1) }} </span>
                                    <span class="sidenav-normal"> {{ __('admin_menu.Majors') }} </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.page', ['settings', 'expertise']) }}"
                                   class="nav-link {{ (request()->is('dashboard/page/expertise')) ? 'active' : '' }}">
                                    <span
                                        class="sidenav-mini-icon"> {{ substr(__('admin_menu.Expertise'), 0, 1) }} </span>
                                    <span class="sidenav-normal"> {{ __('admin_menu.Expertise') }} </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.page', ['settings', 'student-registration']) }}"
                                   class="nav-link {{ (request()->is('dashboard/page/ppdb')) ? 'active' : '' }}">
                                    <span
                                        class="sidenav-mini-icon"> {{ substr(__('admin_menu.student_registration'), 0, 1) }} </span>
                                    <span class="sidenav-normal"> {{ __('admin_menu.student_registration') }} </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('dashboard.page', ['settings', 'roles']) }}"
                                   class="nav-link {{ (request()->is('dashboard/page/roles')) ? 'active' : '' }}">
                                    <span
                                        class="sidenav-mini-icon"> {{ substr(__('admin_menu.roles'), 0, 1) }} </span>
                                    <span class="sidenav-normal"> {{ __('admin_menu.roles') }} </span>
                                </a>
                            </li>
{{--                            <li class="nav-item">--}}
{{--                                <a href="{{ route('dashboard.page', ['settings', 'permissions']) }}"--}}
{{--                                   class="nav-link {{ (request()->is('dashboard/page/permissions')) ? 'active' : '' }}">--}}
{{--                                    <span--}}
{{--                                        class="sidenav-mini-icon"> {{ substr(__('admin_menu.permissions'), 0, 1) }} </span>--}}
{{--                                    <span class="sidenav-normal"> {{ __('admin_menu.permissions') }} </span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
                            <li class="nav-item">
                                <a href="{{ route('dashboard.page', ['settings', 'users']) }}"
                                   class="nav-link {{ (request()->is('dashboard/page/users')) ? 'active' : '' }}">
                                    <span
                                        class="sidenav-mini-icon"> {{ substr(__('admin_menu.users'), 0, 1) }} </span>
                                    <span class="sidenav-normal"> {{ __('admin_menu.users') }} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endif
        </ul>
    </div>
</div>
