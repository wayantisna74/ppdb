<table>
  <tr>
    <th rowspan="2">No</th>
    <th rowspan="2">Kode Pendaftaran</th>
    <th rowspan="2">Siswa</th>
    <th colspan="9">Ayah</th>
    <th colspan="9">Ibu</th>
    <th colspan="9">Wali</th>
  </tr>
  <tr>
    <th>Nama</th>
    <th>NIK</th>
    <th>Alamat</th>
    <th>Tempat Lahir</th>
    <th>Tanggal Lahir</th>
    <th>Pendidikan</th>
    <th>Pekerjaan</th>
    <th>Penghasilan</th>
    <th>Berkebutuhan Khusus</th>

    <th>Nama</th>
    <th>NIK</th>
    <th>Alamat</th>
    <th>Tempat Lahir</th>
    <th>Tanggal Lahir</th>
    <th>Pendidikan</th>
    <th>Pekerjaan</th>
    <th>Penghasilan</th>
    <th>Berkebutuhan Khusus</th>

    <th>Nama</th>
    <th>NIK</th>
    <th>Alamat</th>
    <th>Tempat Lahir</th>
    <th>Tanggal Lahir</th>
    <th>Pendidikan</th>
    <th>Pekerjaan</th>
    <th>Penghasilan</th>
    <th>Berkebutuhan Khusus</th>
  </tr>

  @foreach($datum as $i => $itm)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $itm->ppdb_code }} </td>
      <td>{{ $itm->name }}</td>

      <td>{{ $itm->name_father }}</td>
      <td>{{ $itm->nik_father }}</td>
      <td>{{ $itm->address_father }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->father_born_place : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->father_dob : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_father_education->name : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_father_job->name : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_father_income->value : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_father_special_needs->name : '' }}</td>

      <td>{{ $itm->name_mother }}</td>
      <td>{{ $itm->nik_mother }}</td>
      <td>{{ $itm->address_mother }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->mother_born_place : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->mother_dob : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_mother_education->name : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_mother_job->name : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_mother_income->value : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_mother_special_needs->name : '' }}</td>

      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->guardian_parent_name : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->guardian_parent_nik : '' }}</td>
      <td></td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->guardian_parent_born_place : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->guardian_parent_dob : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_guardian_parent_education->name : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_guardian_parent_job->name : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_guardian_parent_income->value : '' }}</td>
      <td>{{ (isset($itm->data_parent)) ? $itm->data_parent->data_guardian_parent_special_needs->name : '' }}</td>
    </tr>
  @endforeach
</table>
