<table>
  <tr>
    <th rowspan="2">No</th>
    <th rowspan="2">Kode Pendaftaran</th>
    <th rowspan="2">Nama</th>
    <th colspan="5">Bahasa Indonesia</th>
    <th colspan="5">Bahasa Inggris</th>
    <th colspan="5">Matematika</th>
    <th colspan="5">IPA</th>
  </tr>
  <tr>
    @for($i=0; $i < 5; $i++)
      <th>{{ $i }}</th>
    @endfor
    @for($i=0; $i < 5; $i++)
      <th>{{ $i }}</th>
    @endfor
    @for($i=0; $i < 5; $i++)
      <th>{{ $i }}</th>
    @endfor
    @for($i=0; $i < 5; $i++)
      <th>{{ $i }}</th>
    @endfor
  </tr>

  @foreach($datum as $i => $itm)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $itm->ppdb_code }} </td>
      <td>{{ $itm->name }} </td>
      @for($i=1; $i <= 5; $i++)
        @php
          $semester = 'semester_' . $i;
        @endphp
        <th>{{ $itm->reportCards[0]->$semester }}</th>
      @endfor
      @for($i=1; $i <= 5; $i++)
        @php
          $semester = 'semester_' . $i;
        @endphp
        <th>{{ $itm->reportCards[1]->$semester }}</th>
      @endfor
      @for($i=1; $i <= 5; $i++)
        @php
          $semester = 'semester_' . $i;
        @endphp
        <th>{{ $itm->reportCards[2]->$semester }}</th>
      @endfor
      @for($i=1; $i <= 5; $i++)
        @php
          $semester = 'semester_' . $i;
        @endphp
        <th>{{ $itm->reportCards[3]->$semester }}</th>
      @endfor
    </tr>
  @endforeach

</table>
