<table>
  <tr>
    <th>No</th>
    <th>Kode Pendaftaran</th>
    <th>Nama</th>
    <th>Nomor Kartu Kelurga Sejahtera</th>
    <th>Penerima KPS/ PKH</th>
    <th>No KPS/PKH</th>
    <th>Usulan dari Sekolah (Layak PIP)</th>
    <th>Alasan Layak (PIP)</th>
    <th>Penerima KIP</th>
    <th>Penerima Fisik KIP</th>
    <th>Nomor PIP</th>
    <th>Nama di PIP</th>
  </tr>

  @foreach($datum as $i => $itm)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $itm->ppdb_code }} </td>
      <td>{{ $itm->name }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->kks_no : '' }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->is_kps_receiver : '' }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->kps_no : '' }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->is_pip_worthy : '' }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->worthy_reason->name : '' }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->is_kip_receiver : '' }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->is_kip_physical_receiver : '' }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->pip_no : '' }} </td>
      <td>{{ (isset($itm->data_help)) ? $itm->data_help->pip_name : '' }} </td>

    </tr>
  @endforeach

</table>
