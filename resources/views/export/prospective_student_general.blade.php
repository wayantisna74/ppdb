<table>
  <tr>
    <th>No</th>
    <th>Kode Pendaftaran</th>
    <th>Nama</th>
    <th>Tempat/Tgl Lahir</th>
    <th>NISN</th>
    <th>Sekolah Asal</th>
    <th>NIK</th>
    <th>Jenis Kelamin</th>
    <th>Alamat</th>
    <th>Memiliki Kartu KIP?</th>
    <th>No HP</th>
    <th>Jurusan</th>
    <th>Bidang Keahlian</th>
    <th>No Akta Lahir</th>
    <th>Agama &amp; Kepercayaan</th>
    <th>Kebutuhan khusus</th>
    <th>Kewarganegaraan</th>
    <th>Provinsi</th>
    <th>Kabupaten</th>
    <th>Kecamatan</th>
    <th>Desa/kelurahan</th>
    <th>Dusun</th>
    <th>RT</th>
    <th>RW</th>
    <th>Kode Pos</th>
    <th>Tempat tinggal</th>
    <th>Mode Transportasi</th>
    <th>Anak ke</th>
    <th>Jumlah Saudara Kandung</th>
    <th>Golongan Darah</th>
    <th>No Telp Rumah</th>
    <th>Email</th>
    <th>Jenis Extra Kulikuler</th>
    <th>Tinggi badan(cm)</th>
    <th>Lingkar Kepala(cm)</th>
    <th>Berat Badan(Kg)</th>
    <th>Jarak Tempat Tinggal Dengan Sekolah(Km)</th>
    <th>Waktu Tempuh(Menit)</th>
  </tr>

  @foreach($datum as $i => $itm)
    <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $itm->ppdb_code }} </td>
      <td>{{ $itm->name }} </td>
      <td>{{ $itm->born_place. '/'. date('d-M-Y', strtotime($itm->dob)) }} </td>
      <td>{{ $itm->no_nisn }} </td>
      <td>{{ $itm->old_school }} </td>
      <td>{{ $itm->no_nik }} </td>
      <td>{{ $itm->gender }} </td>
      <td>{{ $itm->address }} </td>
      <td>{{ $itm->hasKIP }} </td>
      <td>{{ $itm->no_phone }} </td>
      <td>{{ $itm->major->name }} </td>
      <td>{{ $itm->expertise->name }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->no_bird_card : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->religion->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->spesial_needs->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->nationality : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->province->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->regency->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->district->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->village->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->dusun_name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->rt_name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->rw_name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->zip_code : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->residents->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->transportation->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->family_order : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->sibling_number : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->blood_group->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->home_phone : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->email : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->extracurricular->name : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->height : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->head_circumference : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->weight : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->school_home_distance : '' }} </td>
      <td>{{ (isset($itm->details)) ? $itm->details->travel_time : '' }} </td>
    </tr>
  @endforeach
</table>
