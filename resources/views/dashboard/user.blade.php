@extends('layouts.admin')

@section('content')
    <section class="section">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>{{ __('admin_menu.users') }}</h4>
                        <div class="ml-auto p-2">
                            <button type="button" id="btnNewData" class="btn btn-primary btn-sm text-right">
                                {{ __('admin_menu.add') }}
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped" id="table_users">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        #
                                    </th>
                                    <th>{{ __('auth.fullName') }}</th>
                                    <th>{{ __('Username') }}</th>
                                    <th>{{ __('EMail') }}</th>
                                    <th>{{ __('Role') }}</th>
                                    <th>{{ __('Active') }}</th>
                                    <th>{{ __('admin_menu.Action') }}</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="datatableModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Data</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{-- alert-success show --}}
                                    <div id="alertModal" class="show alert alert-dismissible fade" role="alert">
                                        <span id="alertMessage"></span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form id="formDataUsers" method="POST" action="{{ url('/api/datatable') }}">
                                        <input type="hidden" name="id" id="inputId">
                                        <div class="form-group row">
                                            <label for="inputName" class="col-md-3 col-form-label">{{ __('auth.fullName') }}</label>
                                            <div class="col-md-9">
                                                <input required name="name" type="text" class="form-control" id="inputName"
                                                       placeholder="Name">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPosition" class="col-md-3 col-form-label">Username</label>
                                            <div class="col-md-9">
                                                <input required name="username" type="text" class="form-control" id="inputUsername"
                                                       placeholder="username">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPosition" class="col-md-3 col-form-label">Email</label>
                                            <div class="col-md-9">
                                                <input required name="email" type="email" class="form-control" id="inputEmail"
                                                       placeholder="Email">
                                            </div>
                                        </div>

                                        <div class="form-group row" id="fieldPassword">
                                            <label for="inputPosition" class="col-md-3 col-form-label">Password</label>
                                            <div class="col-md-9">
                                                <input name="password" type="password" class="form-control" id="inputPassword"
                                                       placeholder="Passoword" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPosition" class="col-md-3 col-form-label">Role</label>
                                            <div class="col-md-9">
                                                <select class="form-control"
                                                        name="role_id" required
                                                        id="role_id">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPosition" class="col-md-3 col-form-label">Active</label>
                                            <div class="col-md-9">
                                                <select class="form-control"
                                                        name="is_active" required
                                                        id="is_active">
                                                    <option value="Y">Ya</option>
                                                    <option value="N">Tidak</option>
                                                </select>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" form="formDataUsers" id="submitBtn" class="btn btn-primary">
                                        <i id="fa_spin" class="fas fa-spinner fa-spin"></i>
                                        {{ __('admin_menu.save') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')
    <script>
        function calcAverage(params, averageInput) {
            var total = 0;
            var arr_semester = [];
            for (var i = 1; i <= 5; i++) {
                var semester = $('#' + params + i).val().replace(',', '');
                var semesterVal
                if (semester && !isNaN(semester)) {
                    semesterVal = parseFloat(semester)
                } else {
                    semesterVal = 0
                }
                arr_semester.push(semesterVal)
                //console.log(isNaN(semester))
                total += parseFloat(semesterVal);
            }
            var average = parseFloat(total) / 5;
            $('#' + averageInput).val(average)
        }

        $(document).ready(function () {
            $('.flatpickr2').flatpickr({
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d",
                maxDate: "@php echo(date('Y')-10).'-12' @endphp",
                minDate: "@php echo(date('Y')-21).'-01' @endphp"
            });

            // initializing Datatable
            var table = $("#table_users").DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: window.app + "/users",
                    type: "GET",
                },
                language: {
                    paginate: {
                        previous: "<i class='fas fa-angle-left'>",
                        next: "<i class='fas fa-angle-right'>"
                    }
                },
                "columns": [
                    {
                        data: "no",
                        name: "no"
                    },
                    {
                        data: "name",
                        name: "name"
                    },
                    {
                        data: "username",
                        name: "username"
                    },
                    {
                        data: "email",
                        name: "email"
                    },
                    {
                        data: "role",
                        name: "role"
                    },
                    {
                        data: "is_active",
                        name: "is_active"
                    },
                    {
                        data: "action",
                        name: "action",
                        orderable: false
                    }]
            });

            $('#table_users tbody').on('click', 'button', function () {
                window.rowData = table.row($(this).parents('tr')).data();
                var id = $(this).data('id')

                if ($(this).data('id-btn') === 'btn-info' + window.rowData.id) {
                    $("#btn_major_add" + window.rowData.id).click(viewTable(window.rowData))
                } else if ($(this).data('id-btn') === 'btn-danger' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(deleteRow(window.rowData.id))
                } else if ($(this).data('id-btn') === 'btn-warning' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(editTable(window.rowData))
                }
            });

            // Edit the data
            function editTable(rowData) {
                $('#fa_spin').hide()
                $("#alertModal").toggleClass("display-none");
                $("#alertModal").addClass("display-none").removeClass("alert-danger");
                $("#datatableModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });

                $("#inputName").val(rowData.name);
                $("#inputUsername").val(rowData.username);
                $("#inputEmail").val(rowData.email);
                $("#role_id select").val(rowData.role_id);
                console.log(rowData.role_id)
            }

            // Delete row in datatable
            function deleteRow(id) {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: window.app + '/users/' + id,
                            type: 'DELETE',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.error) {
                                    $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                                    $("#alertMsgHome").html(data.msg);
                                    setTimeout(function () {
                                        $("#alertHome").addClass("display-none").removeClass("alert-success")
                                    }, 3000);
                                } else {
                                    swalWithBootstrapButtons.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    )
                                    $("#table_users").DataTable().ajax.reload();
                                }
                            }
                        });
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                })
            }
            getRole()
            function getRole() {
                $.ajax({
                    url: window.app+'/role-user',
                    type: 'GET',
                    dataType: 'JSON',
                    success: function(result) {
                        if (result) {
                            $.each(result, function (key, value) {
                                $('#role_id')
                                    .append($("<option></option>")
                                        .attr("value", value.id)
                                        .text(value.name));
                            })
                        }
                    }
                })
            }

            // Display modal for add new data
            $("#btnNewData").click(function (e) {
                e.preventDefault();

                $('#fa_spin').hide()
                $("#alertModal").addClass("display-none").removeClass("alert-danger")
                $("#inputId").val(null)
                $("#datatableModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $('#selectParams').on('change', function () {
                console.log(this.value)
                if (this.value === 'age' || this.value === 'salary') {
                    $('#inputDateFirst').hide();
                    $('#inputDateSecond').hide();
                    $('#inputNumFirst').show();
                    $('#inputNumSecond').show();
                } else if (this.value === 'salary') {
                    $('#inputDateFirst').hide();
                    $('#inputDateSecond').hide();
                    $('#inputNumFirst').show();
                    $('#inputNumSecond').show();
                } else if (this.value === 'start_work') {
                    $('#inputNumFirst').hide();
                    $('#inputNumSecond').hide();
                    $('#inputDateFirst').show();
                    $('#inputDateSecond').show();
                }
            });

            // Display modal for add new data
            $("#btnExport").click(function (e) {
                e.preventDefault();
                $('#inputDateFirst').hide();
                $('#inputDateSecond').hide();
                $('#inputNumFirst').show();
                $('#inputNumSecond').show();

                var startDate = $("#inputStartDateExport").val('');
                var endDate = $("#inputEndDateExport").val('');
                var startNum = $("#inputStartNum").val('');
                var endNum = $("#inputEndNum").val('');

                $("#modalExport").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            // submit export
            $("#formDataExport").submit(function (e) {
                e.preventDefault();
                var selectVal = $("#selectParams option:selected").val();
                var startDate = $("#inputStartDateExport").val();
                var endDate = $("#inputEndDateExport").val();
                var startNum = $("#inputStartNum").val();
                var endNum = $("#inputEndNum").val();
                $.ajax({
                    url: window.app + '/users-export',
                    method: 'GET',
                    dataType: 'JSON',
                    data: {
                        select: selectVal,
                        startDate: startDate,
                        endDate: endDate,
                        startNum: startNum,
                        endNum: endNum,
                    },
                    success: function (result) {
                        window.location = result.url
                    },
                    error: function (request, textStatus, errorThrown) {
                        console.log("failed: " + textStatus + " / " + errorThrown);
                    }
                })
            });

            $("#btnRefresh").click(function (e) {
                e.preventDefault();
                $("#table_users").DataTable().ajax.reload();
            });

            // When submit the form
            $('#formDataUsers').submit(function (e) {
                e.preventDefault()
                var form = $(this),
                    url = form.attr("action")
                var formData = form.serialize();
                $('#fa_spin').show()
                $.ajax({
                    url: window.app + "/users",
                    data: formData,
                    dataType: 'json', // data type
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                        $('#fa_spin').hide()
                        if (data.error) {
                            $("#alertModal").toggleClass("display-none").toggleClass("alert-danger");
                            var message = 0;
                            var list_message = ''
                            $.each(data.msg, function (key, value) {
                                list_message += data.msg[key][0] + ', '
                                message++;
                            })
                            $("#alertMessage").html(list_message);
                            setTimeout(function () {
                                $("#alertModal").addClass("display-none").removeClass("alert-danger")
                            }, 5000);
                        } else {
                            $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                            setTimeout(function () {
                                $("#alertHome").addClass("display-none").removeClass("alert-success")
                            }, 3000);
                            $("#alertMsgHome").html(data.msg);
                            $("#datatableModal").modal("hide");
                            $("#table_users").DataTable().ajax.reload();
                        }
                    }
                });
            });

            // Reset form when close
            $('#datatableModal').on('hidden.bs.modal', function () {
                $('#datatableModal form')[0].reset();
            });

            function deleteItem(id) {

            };
        });

    </script>
@endsection
