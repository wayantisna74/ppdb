@extends('layouts.admin')

@section('content')
    <section class="section">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4>Image Check</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label">Image Check</label>
                            <div class="row gutters-sm">
                                <div class="col-6 col-sm-4">
                                    <label class="imagecheck mb-4">
                                        <input name="imagecheck" type="checkbox" value="1" class="imagecheck-input"  />
                                        <figure class="imagecheck-figure">
                                            <img src="../assets/img/news/img01.jpg" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                                <div class="col-6 col-sm-4">
                                    <label class="imagecheck mb-4">
                                        <input name="imagecheck" type="checkbox" value="2" class="imagecheck-input"  checked />
                                        <figure class="imagecheck-figure">
                                            <img src="../assets/img/news/img02.jpg" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                                <div class="col-6 col-sm-4">
                                    <label class="imagecheck mb-4">
                                        <input name="imagecheck" type="checkbox" value="3" class="imagecheck-input"  />
                                        <figure class="imagecheck-figure">
                                            <img src="../assets/img/news/img03.jpg" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                                <div class="col-6 col-sm-4">
                                    <label class="imagecheck mb-4">
                                        <input name="imagecheck" type="checkbox" value="4" class="imagecheck-input"  checked />
                                        <figure class="imagecheck-figure">
                                            <img src="../assets/img/news/img04.jpg" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                                <div class="col-6 col-sm-4">
                                    <label class="imagecheck mb-4">
                                        <input name="imagecheck" type="checkbox" value="5" class="imagecheck-input"  />
                                        <figure class="imagecheck-figure">
                                            <img src="../assets/img/news/img05.jpg" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                                <div class="col-6 col-sm-4">
                                    <label class="imagecheck mb-4">
                                        <input name="imagecheck" type="checkbox" value="6" class="imagecheck-input"  />
                                        <figure class="imagecheck-figure">
                                            <img src="../assets/img/news/img06.jpg" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4>Input Text</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Default Input Text</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Purchase Code</label>
                            <input type="text" class="form-control purchase-code" placeholder="ASDF-GHIJ-KLMN-OPQR">
                        </div>
                        <div class="form-group">
                            <label>Invoice</label>
                            <input type="text" class="form-control invoice-input">
                        </div>
                        <div class="form-group">
                            <label>Date</label>
                            <input type="text" class="form-control datemask" placeholder="YYYY/MM/DD">
                        </div>
                        <div class="form-group">
                            <label>Credit Card</label>
                            <input type="text" class="form-control creditcard">
                        </div>
                        <div class="form-group">
                            <label>Tags</label>
                            <input type="text" class="form-control inputtags">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
