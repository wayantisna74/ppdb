@extends('layouts.admin')

@section('content')
    <!-- Card stats -->
    @if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Superuser')
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card card-stats">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Total Pendaftar PPDB</h5>
                                <span class="h2 font-weight-bold mb-0">{{ $pending + $reregistration + $reject }}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                    <i class="ni ni-active-40"></i>
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                            <span class="text-success mr-2">
                                {{ $pending }}</span>
                            <span class="text-nowrap">Tunda</span>
                        </p>
                        <p class="mt-3 mb-0 text-sm">
                            <span class="text-success mr-2">{{ $reregistration }}</span>
                            <span class="text-nowrap">Daftar Ulang</span>
                        </p>
                        <p class="mt-3 mb-0 text-sm">
                            <span class="text-success mr-2">{{ $reject }}</span>
                            <span class="text-nowrap">Ditolak</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Peserta PPDB</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('dashboard.page',['registration', 'prospective-students']) }}"
                                   class="btn btn-sm btn-primary">Lebih lengkapl</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <tr>
                                <th>ID Pendaftaran</th>
                                <th>Nama</th>
                                <th>Jurusan</th>
                                <th>Status</th>
                                <th>Tanggal Daftar</th>
                            </tr>
                            @foreach($students as $student)
                                <tr>
                                    <td>{{ $student->ppdb_code }}</td>
                                    <td class="font-weight-600">{{ $student->name }}</td>
                                    <td class="font-weight-600">{{ $student->major->name .'/'.$student->expertise->name }}</td>
                                    <td>
                                        @if($student->approval_step == 'P')
                                            <div class="badge badge-warning">Pending</div>
                                        @elseif($student->approval_step == 'R')
                                            <div class="badge badge-danger">Reject</div>
                                        @elseif($student->approval_step == 'G')
                                            <div class="badge badge-info">Re Registration</div>
                                        @endif
                                    </td>
                                    <td>{{ date('d-M-Y H:i:s', strtotime($student->created_at)) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @elseif(auth()->user()->student->approval_step == 'G')
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card card-stats">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Selamat datang,</h5>
                                <span class="h5 font-weight-bold mb-0">Silahkan lengkapi data untuk pendaftaran ulang</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                    <i class="ni ni-active-40"></i>
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                            <span class="text-nowrap">
                                <a class="btn btn-success btn-sm" href="{{ route('student.reregistration') }}">Daftar Ulang</a>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    @endif

@endsection
