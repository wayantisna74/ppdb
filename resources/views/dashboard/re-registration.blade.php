@extends('layouts.admin')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <h4>Data Registrasi Ulang</h4>
                            </div>
                            <div class="col-6 text-right">
                                <button onclick="sendData()" class="btn btn-primary">Kirim</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table_re_registration">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Tahap Pengisian</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    {{--                    modal upload Photo --}}
                    <div class="modal fade" id="modal_photo" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Upload Foto</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <form id="formDataReRegistration" method="POST" action="{{ route('store.student.photo') }}"
                                      enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group row">
                                            <label for="inputName"
                                                   class="col-md-3 col-form-label">{{ __('Foto') }}</label>
                                            <div class="col-md-9">
                                                <input required name="file" type="file" class="form-control"
                                                       id="inputFile"
                                                       placeholder="Foto">
                                            </div>
                                        </div>
                                        <div class="form_img">
                                            <img src="" alt="" class="img img-fluid" id="img_photo">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button onclick='submitActionPhoto()' type="submit" id="submitBtn"
                                                class="btn btn-success">
                                            <i id="fa_spin" class="fas fa-spinner fa-spin"></i>
                                            {{ __('admin_menu.save') }}`
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    {{--modal details--}}
    <div class="modal fade" id="modal_details" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered"
             role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal_details">Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <span id="show_loading" class="text-center">

                </span>
                <div id="content_modal"></div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.flatpickr2').flatpickr({
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d"
            });

            // initializing Datatable
            var table = $("#table_re_registration").DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "searching": false,
                "dom": 'rt',
                "ajax": {
                    url: window.app + "/re-registration",
                    type: "GET",
                },
                language: {
                    paginate: {
                        previous: "<i class='fas fa-angle-left'>",
                        next: "<i class='fas fa-angle-right'>"
                    }
                },
                "columns": [
                    {
                        data: null,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                        name: "no"
                    },
                    {
                        data: "step_desc",
                        name: "step_desc"
                    },
                    {
                        data: "status",
                        name: "status"
                    },
                    {
                        data: "action",
                        name: "action",
                    }]
            });

            function showPhoto() {
                $.ajax({
                    url: window.app + "/student-photo",
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.src !== undefined) {
                            $('#img_photo').attr('src', data.src)
                        }
                    }
                });
            }

            function showDetails(type) {
                var url = '';
                switch (type) {
                    case 'details':
                        url = window.app + "/student-details";
                        break;
                    case 'help':
                        url = window.app + "/student-data-help";
                        break;
                    case 'parent':
                        url = window.app + "/student-data-parent";
                        break;
                    case 'score':
                        url = window.app + "/student-score";
                        break;
                    case 'files':
                        url = window.app + "/student-files";
                        break;
                }
                $("#content_modal").empty();
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        $('#show_loading').empty()
                        $("#content_modal").append(data)

                        var myInput = document.querySelector(".flatpickr2");
                        if(myInput !== null) {
                            var fp = flatpickr(myInput, {
                                altInput: true,
                                altFormat: "F j, Y",
                                dateFormat: "Y-m-d",
                                defaultDate: "@php echo(date('Y')-16).'-12-31' @endphp",
                                maxDate: "@php echo(date('Y')-21).'-01' @endphp"
                            })

                            var myInput2 = document.querySelector(".flatpickr3");
                            var fp2 = flatpickr(myInput2, {
                                altInput: true,
                                altFormat: "F j, Y",
                                dateFormat: "Y-m-d",
                                defaultDate: "@php echo(date('Y')-16).'-12-31' @endphp",
                                maxDate: "@php echo(date('Y')-21).'-01' @endphp"
                            })

                            var myInput3 = document.querySelector(".flatpickr4");
                            var fp3 = flatpickr(myInput3, {
                                altInput: true,
                                altFormat: "F j, Y",
                                dateFormat: "Y-m-d",
                                defaultDate: "@php echo(date('Y')-16).'-12-31' @endphp",
                                maxDate: "@php echo(date('Y')-21).'-01' @endphp"
                            })

                            fp.setDate($('#father_dob').val(), true)
                            fp2.setDate($('#mother_dob').val(), true)
                            fp3.setDate($('#guardian_parent_dob').val(), true)
                        }

                    },
                });
            }

            function showModal(rowData, type) {
                $('#show_loading').empty()
                if (type === 'photo') {
                    showPhoto();
                    $("#modal_photo").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $("#submitBtn").text('Simpan')
                } else if (type === 'details' || type === 'help' || type === 'parent' || type === 'score' || type === 'files') {
                    $('#show_loading').append('<i id="spin_load" class="p-3 fa fa-spinner fa-4x fa-spin" ></i>');
                    showDetails(type)
                    $("#modal_details").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }

            $('#table_re_registration tbody').on('click', 'button', function () {
                window.rowData = table.row($(this).parents('tr')).data();
                var id = $(this).data('id')
                switch ($(this).data('id-btn')) {
                    case 'btn_photo':
                        $("#btn_photo" + window.rowData.id).click(showModal(window.rowData, 'photo'));
                        break;
                    case 'btn_details':
                        $("#btn_details" + window.rowData.id).click(showModal(window.rowData, 'details'));
                        break;
                    case 'btn_help':
                        $("#btn_help" + window.rowData.id).click(showModal(window.rowData, 'help'));
                        break;
                    case 'btn_parent':
                        $("#btn_parent" + window.rowData.id).click(showModal(window.rowData, 'parent'));
                        break;
                    case 'btn_score':
                        $("#btn_score" + window.rowData.id).click(showModal(window.rowData, 'score'));
                        break;
                    case 'btn_file':
                        $("#btn_file" + window.rowData.id).click(showModal(window.rowData, 'files'));
                        break;
                }
            });
        });

        function changeProvince(value) {
            $.ajax({
                url: window.app + "/regency",
                type: 'GET',
                data: {
                    province_id: value
                },
                dataType: 'JSON',
                success: function (data) {
                    $('#regency_id').empty()
                    $('#district_id').empty()
                    $('#village_id').empty()
                    $('#regency_id').append(data)
                }
            });
        }

        function changeRegency(value) {
            $.ajax({
                url: window.app + "/districts",
                type: 'GET',
                data: {
                    regency_id: value
                },
                dataType: 'JSON',
                success: function (data) {
                    $('#district_id').empty()
                    $('#village_id').empty()
                    $('#district_id').append(data)
                }
            });
        }

        function changeDistrict(value) {
            $.ajax({
                url: window.app + "/villages",
                type: 'GET',
                data: {
                    district_id: value
                },
                dataType: 'JSON',
                success: function (data) {
                    $('#village_id').empty()
                    $('#village_id').append(data)
                }
            });
        }

        $('#formDataReRegistration').on('submit', function(event) {
            event.preventDefault();
            $("#submitBtn").text('Menyimpan...').append(' <i id="spin_load" class="fa fa-spinner fa-spin" ></i>')
            $.ajax({
                url: $('#formDataReRegistration').attr('action'),
                data: new FormData(this),
                method: 'POST',
                processData: false,
                dataType:'JSON',
                contentType: false,
                cache: false,
                success: function (result) {
                    $('#spin_load').hide();
                    $("#submitBtn").text('Simpan')
                    if (result.errors) {
                        Swal.fire({
                            title: 'Error!',
                            text: result.message,
                            icon: 'error',
                        })
                    } else {
                        $("#table_re_registration").DataTable().ajax.reload();
                        $("#submitBtn").val('')
                        $('#modal_photo').modal('hide');
                        Swal.fire({
                            title: 'Success!',
                            text: result.message,
                            icon: 'success',
                        })
                    }
                },
                error: function (request, textStatus, errorThrown) {
                    $("#submitBtn").text('Simpan')
                    $('#spin_load').hide();
                    var text = (request.responseJSON) ? request.responseJSON.message : errorThrown + " / " + textStatus;
                    Swal.fire({
                        title: 'Error!',
                        text: text,
                        icon: 'error',
                    })
                }
            });
        });

        function submitActionPhoto() {
            var form = $('form#formDataReRegistration')

        }

        function submitAction() {
            var form = $('form#form_details')
            $("#submitBtnAction").text('Menyimpan...').append(' <i id="spin_load" class="fa fa-spinner fa-spin" ></i>')
            $.ajax({
                url: form.attr('action'),
                data: form.serializeArray(),
                type: 'POST',
                dataType: "json",
                success: function (result) {
                    $('#spin_load').hide();
                    $("#submitBtnAction").text('Simpan')
                    if (result.errors) {
                        Swal.fire({
                            title: 'Error!',
                            text: result.message,
                            icon: 'error',
                        })
                    } else {
                        $("#table_re_registration").DataTable().ajax.reload();
                        $("#submitBtnAction").val('')
                        $('#modal_details').modal('hide');
                        Swal.fire({
                            title: 'Success!',
                            text: result.message,
                            icon: 'success',
                        })
                    }
                },
                error: function (request, textStatus, errorThrown) {
                    $("#submitBtnAction").text('Simpan')
                    $('#spin_load').hide();
                    var text = (request.responseJSON) ? request.responseJSON.message : errorThrown + " / " + textStatus;
                    Swal.fire({
                        title: 'Error!',
                        text: text,
                        icon: 'error',
                    })
                }
            });
        }

        function sendData() {
            var swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Anda yakin?',
                text: "Data yang sudah dikirim tidak bisa di ubah lagi",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, yakin!',
                cancelButtonText: 'Tidak, batal!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: window.app + '/student-send-data/',
                        type: 'post',
                        dataType: 'JSON',
                        success: function (data) {
                            if (data.error) {
                                Swal.fire(
                                    'Perhatian!',
                                    data.message,
                                    'warning'
                                )
                            } else {
                                swalWithBootstrapButtons.fire(
                                    'Berhasil!',
                                    'Data telah dikirim',
                                    'success'
                                )
                                $("#table_re_registration").DataTable().ajax.reload();
                            }
                        },
                        error: function (request, textStatus, errorThrown) {
                            swalWithBootstrapButtons.fire({
                                title: 'Error!',
                                text:  errorThrown + " / " + textStatus,
                                icon: 'error',
                            })
                        }
                    });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Dibatalkan',
                        'Proses dibatalkan!',
                        'error'
                    )
                }
            })
        }
    </script>
@endsection
