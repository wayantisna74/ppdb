@extends('layouts.admin')

@section('content')
    <section class="section">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>{{ __('admin_menu.roles') }}</h4>
                        <div class="ml-auto p-2">
                            <button type="button" id="btnNewData" class="btn btn-primary btn-sm text-right">
                                {{ __('admin_menu.add') }}
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped" id="table_roles">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        #
                                    </th>
                                    <th>{{ __('Nama') }}</th>
                                    <th>{{ __('admin_menu.Action') }}</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="datatableModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Data</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{-- alert-success show --}}
                                    <div id="alertModal" class="show alert alert-dismissible fade" role="alert">
                                        <span id="alertMessage"></span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form id="formDataRoles" method="POST" action="{{ url('/api/datatable') }}">
                                        <input type="hidden" name="id" id="inputId">
                                        <div class="form-group row">
                                            <label for="inputName" class="col-md-3 col-form-label">{{ __('Nama') }}</label>
                                            <div class="col-md-9">
                                                <input required name="name" type="text" class="form-control" id="inputName"
                                                       placeholder="Name">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" form="formDataRoles" id="submitBtn" class="btn btn-primary">
                                        <i id="fa_spin" class="fas fa-spinner fa-spin"></i>
                                        {{ __('admin_menu.save') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        function calcAverage(params, averageInput) {
            var total = 0;
            var arr_semester = [];
            for (var i = 1; i <= 5; i++) {
                var semester = $('#' + params + i).val().replace(',', '');
                var semesterVal
                if (semester && !isNaN(semester)) {
                    semesterVal = parseFloat(semester)
                } else {
                    semesterVal = 0
                }
                arr_semester.push(semesterVal)
                //console.log(isNaN(semester))
                total += parseFloat(semesterVal);
            }
            var average = parseFloat(total) / 5;
            $('#' + averageInput).val(average)
        }

        $(document).ready(function () {
            $('.flatpickr2').flatpickr({
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d",
                maxDate: "@php echo(date('Y')-10).'-12' @endphp",
                minDate: "@php echo(date('Y')-21).'-01' @endphp"
            });

            // initializing Datatable
            var table = $("#table_roles").DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: window.app + "/roles",
                    type: "GET",
                },
                language: {
                    paginate: {
                        previous: "<i class='fas fa-angle-left'>",
                        next: "<i class='fas fa-angle-right'>"
                    }
                },
                "columns": [
                    {
                        data: "no",
                        name: "no"
                    },
                    {
                        data: "name",
                        name: "name"
                    },
                    {
                        data: "action",
                        name: "action",
                        orderable: false
                    }]
            });

            $('#table_roles tbody').on('click', 'button', function () {
                window.rowData = table.row($(this).parents('tr')).data();
                var id = $(this).data('id')

                if ($(this).data('id-btn') === 'btn-info' + window.rowData.id) {
                    $("#btn_major_add" + window.rowData.id).click(viewTable(window.rowData))
                } else if ($(this).data('id-btn') === 'btn-danger' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(deleteRow(window.rowData.id))
                } else if ($(this).data('id-btn') === 'btn-warning' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(editTable(window.rowData.id))
                }
            });

            // Edit the data
            function editTable(id) {
                $('#fa_spin').hide()
                $("#alertModal").toggleClass("display-none");
                $("#alertModal").addClass("display-none").removeClass("alert-danger");
                $("#inputId").val(id);
                $("#datatableModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $.ajax({
                    url: window.app + "/roles/" + id,
                    dataType: 'json', // data type
                    type: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        $("#inputName").val(data.row.name);
                        $("#inputDescription").val(data.row.description);
                    }
                });
            }

            // Delete row in datatable
            function deleteRow(id) {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: window.app + '/roles/' + id,
                            type: 'DELETE',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.error) {
                                    $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                                    $("#alertMsgHome").html(data.msg);
                                    setTimeout(function () {
                                        $("#alertHome").addClass("display-none").removeClass("alert-success")
                                    }, 3000);
                                } else {
                                    swalWithBootstrapButtons.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    )
                                    $("#table_roles").DataTable().ajax.reload();
                                }
                            }
                        });
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                })
            }

            // Display modal for add new data
            $("#btnNewData").click(function (e) {
                e.preventDefault();
                $('#fa_spin').hide()
                $("#alertModal").addClass("display-none").removeClass("alert-danger")
                $("#inputId").val(null)
                $("#datatableModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $('#selectParams').on('change', function () {
                console.log(this.value)
                if (this.value === 'age' || this.value === 'salary') {
                    $('#inputDateFirst').hide();
                    $('#inputDateSecond').hide();
                    $('#inputNumFirst').show();
                    $('#inputNumSecond').show();
                } else if (this.value === 'salary') {
                    $('#inputDateFirst').hide();
                    $('#inputDateSecond').hide();
                    $('#inputNumFirst').show();
                    $('#inputNumSecond').show();
                } else if (this.value === 'start_work') {
                    $('#inputNumFirst').hide();
                    $('#inputNumSecond').hide();
                    $('#inputDateFirst').show();
                    $('#inputDateSecond').show();
                }
            });

            // Display modal for add new data
            $("#btnExport").click(function (e) {
                e.preventDefault();
                $('#inputDateFirst').hide();
                $('#inputDateSecond').hide();
                $('#inputNumFirst').show();
                $('#inputNumSecond').show();

                var startDate = $("#inputStartDateExport").val('');
                var endDate = $("#inputEndDateExport").val('');
                var startNum = $("#inputStartNum").val('');
                var endNum = $("#inputEndNum").val('');

                $("#modalExport").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            // submit export
            $("#formExport").submit(function (e) {
                e.preventDefault();
                var selectVal = $("#selectParams option:selected").val();
                var startDate = $("#inputStartDateExport").val();
                var endDate = $("#inputEndDateExport").val();
                var startNum = $("#inputStartNum").val();
                var endNum = $("#inputEndNum").val();
                $.ajax({
                    url: window.app + '/roles-export',
                    method: 'GET',
                    dataType: 'JSON',
                    data: {
                        select: selectVal,
                        startDate: startDate,
                        endDate: endDate,
                        startNum: startNum,
                        endNum: endNum,
                    },
                    success: function (result) {
                        window.location = result.url
                    },
                    error: function (request, textStatus, errorThrown) {
                        console.log("failed: " + textStatus + " / " + errorThrown);
                    }
                })
            });

            $("#btnRefresh").click(function (e) {
                e.preventDefault();
                $("#table_roles").DataTable().ajax.reload();
            });

            // When submit the form
            $('#formDataRoles').submit(function (e) {
                e.preventDefault()
                var form = $(this),
                    url = form.attr("action")
                var formData = form.serialize();
                $('#fa_spin').show()
                $.ajax({
                    url: window.app + "/roles",
                    data: formData,
                    dataType: 'json', // data type
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                        $('#fa_spin').hide()
                        if (data.error) {
                            $("#alertModal").toggleClass("display-none").toggleClass("alert-danger");
                            $("#alertMessage").html(data.msg);
                            setTimeout(function () {
                                $("#alertModal").addClass("display-none").removeClass("alert-danger")
                            }, 3000);
                        } else {
                            $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                            setTimeout(function () {
                                $("#alertHome").addClass("display-none").removeClass("alert-success")
                            }, 3000);
                            $("#alertMsgHome").html(data.msg);
                            $("#datatableModal").modal("hide");
                            $("#table_roles").DataTable().ajax.reload();
                        }
                    }
                });
            });

            // Reset form when close
            $('#datatableModal').on('hidden.bs.modal', function () {
                $('#datatableModal form')[0].reset();
            });

            function deleteItem(id) {

            };
        });

    </script>
@endsection
