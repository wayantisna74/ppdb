<div class="modal fade" id="modal_parent" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered"
         role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal_details">Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{ route('re_registration@details') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body">
                    {{-- alert-success show --}}
                    <div class="cards">
                        <div class="card-bodys">
                            <span class="section-title mt-0">Data Pribadi</span>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="no_bird_card">{{ __('No Akta Lahir') }}</label>
                                    <input type="text" id="no_bird_card" class="form-control form-control-sm  " name="name" required autofocus>
                                </div>

                                <div class="form-group col-md-4 col-6">
                                    <label for="religion_id">{{ __('Agama & Kepercayaan') }}</label>
                                    <select name="religion_id" id="religion_id" class="form-control form-control-sm  ">
                                        @foreach($religions AS $religion)
                                            <option value="{{ $religion->id }}">{{ $religion->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-4 col-6">
                                    <label for="special_need_id">{{ __('Kebutuhan khusus') }}</label>
                                    <select name="special_need_id" id="special_need_id" class="form-control form-control-sm  ">
                                        @foreach($special_needs AS $special_need)
                                            <option value="{{ $special_need->id }}">{{ $special_need->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="nationality">{{ __('Kewarganegaraan') }}</label>
                                    <input type="text" minlength="10" maxlength="10"
                                           id="nationality"
                                           class="form-control form-control-sm  @error('no_nisn') is-invalid @enderror"
                                           name="nationality" required autofocus>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="province_id">{{ __('Provinsi') }}</label>
                                    <select name="province_id" id="province_id" class="form-control form-control-sm  ">
                                        @foreach($provinces AS $province)
                                            <option value="{{ $province->id }}">{{ $province->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="regency_id">{{ __('Kabupaten') }}</label>
                                    <select name="regency_id" id="regency_id" class="form-control form-control-sm  ">
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="district_id">{{ __('Kecamatan') }}</label>
                                    <select name="district_id" id="district_id" class="form-control form-control-sm  ">
                                    </select>
                                </div>

                                <div class="form-group col-md-8">
                                    <label for="village_id">{{ __('Desa/kelurahan') }}</label>
                                    <select name="village_id" id="village_id" class="form-control form-control-sm  ">
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="dusun_name">{{ __('Dusun') }}</label>
                                    <input type="text" minlength="16" maxlength="16" id="dusun_name"
                                           class="form-control form-control-sm  @error('hasKIP') is-invalid @enderror"
                                           name="dusun_name" required value="{{ old('hasKIP') }}">

                                </div>

                                <div class="form-group col-md-8">
                                    <label for="rt_name">{{ __('RT') }}</label>
                                    <input type="text" minlength="12" maxlength="13" id="rt_name"
                                           class="form-control form-control-sm  @error('no_phone') is-invalid @enderror"
                                           name="rt_name" value="{{ old('no_phone') }}" required>
                                </div>

                                <div class="form-group col-md-8">
                                    <label for="rw_name">{{ __('RW') }}</label>
                                    <input type="text" minlength="12" maxlength="13" id="rw_name"
                                           class="form-control form-control-sm  @error('no_phone') is-invalid @enderror"
                                           name="rw_name" value="{{ old('no_phone') }}" required>
                                </div>

                                <div class="form-group col-md-8">
                                    <label for="zip_code">{{ __('Kodepos') }}</label>
                                    <input type="text" minlength="12" maxlength="13" id="zip_code"
                                           class="form-control form-control-sm  @error('no_phone') is-invalid @enderror"
                                           name="zip_code" value="{{ old('no_phone') }}" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="residence_id">{{ __('Tempat tinggal') }}</label>
                                    <select name="residence_id" id="special_need_id" class="form-control form-control-sm  ">
                                        @foreach($residences AS $residence)
                                            <option value="{{ $residence->id }}">{{ $residence->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="transportation_id">{{ __('Mode Transportasi') }}</label>
                                    <select name="transportation_id" id="transportation_id" class="form-control form-control-sm  ">
                                        @foreach($transportations AS $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="family_order">{{ __('Anak ke') }}</label>
                                    <input type="text" id="family_order"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="family_order" value="{{ old('average_ipa') }}"
                                           required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="sibling_number">{{ __('Jumlah Saudara Kandung') }}</label>
                                    <input type="text" id="sibling_number"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="sibling_number" value="{{ old('average_ipa') }}"
                                           required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">{{ __('Golongan Darah') }}</label>
                                    <select name="transportation_id" id="transportation_id" class="form-control form-control-sm  ">
                                        @foreach($blood_groups AS $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="home_phone">{{ __('No Telp Rumah') }}</label>
                                    <input type="text" id="home_phone"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="home_phone">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="email">{{ __('Email') }}</label>
                                    <input type="text" id="email"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="email" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="extracurricular_id">{{ __('Jenis Extra Kulikuler') }}</label>
                                    <select name="extracurricular_id" id="extracurricular_id" class="form-control form-control-sm  ">
                                        @foreach($extracurricular AS $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">{{ __('Tinggi badan') }}</label>
                                    <input type="text" id="height"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="height" value="{{ old('average_ipa') }}"
                                           required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="head_circumference">{{ __('Lingkar Kepala') }}</label>
                                    <input type="text" id="head_circumference"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="head_circumference" value="{{ old('average_ipa') }}"
                                           required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="weight">{{ __('Berat Badan') }}</label>
                                    <input type="text" id="weight"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="weight" value="{{ old('average_ipa') }}"
                                           required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="school_home_distance">{{ __('Jarak Tempat Tinggal Dengan Sekolah') }}</label>
                                    <input type="text" id="school_home_distance"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="school_home_distance" value="{{ old('average_ipa') }}"
                                           required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="travel_time">{{ __('Waktu Tempuh') }}</label>
                                    <input type="text" id="travel_time"
                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                           name="travel_time" value="{{ old('average_ipa') }}"
                                           required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                    </button>
                    <button type="submit" form="formDataProspecStudent" id="submitBtn"
                            class="btn btn-success">
                        <i id="fa_spin" class="fas fa-spinner fa-spin"></i>
                        {{ __('admin_menu.Re-registration') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
