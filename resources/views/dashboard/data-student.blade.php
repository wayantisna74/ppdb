@extends('layouts.admin')

@section('content')
    <section class="section">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>{{ __('admin_menu.majors') }}</h4>
                        <div class="ml-auto p-2">
                            <button type="button" id="btnNewData" class="btn btn-primary btn-sm text-right">
                                {{ __('admin_menu.add') }}
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped" id="table_expertise">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        #
                                    </th>
                                    <th>{{ __('admin_menu.Majors') }}</th>
                                    <th>{{ __('admin_menu.Description') }}</th>
                                    <th>{{ __('admin_menu.Action') }}</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="datatableModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Data</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{-- alert-success show --}}
                                    <div id="alertModal" class="show alert alert-dismissible fade" role="alert">
                                        <span id="alertMessage"></span>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form id="formDataStudents" method="POST" action="{{ url('/api/datatable') }}">
                                        <input type="hidden" name="id" id="inputId">
                                        <div class="form-group row">
                                            <label for="inputName" class="col-md-3 col-form-label">{{ __('admin_menu.majors') }}</label>
                                            <div class="col-md-9">
                                                <input required name="name" type="text" class="form-control" id="inputName"
                                                       placeholder="Name">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputPosition" class="col-md-3 col-form-label">Deskripsi</label>
                                            <div class="col-md-9">
                                                <input required name="description" type="text" class="form-control" id="inputDescription"
                                                       placeholder="Description">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" form="formDataStudents" id="submitBtn" class="btn btn-primary">
                                        <i id="fa_spin" class="fas fa-spinner fa-spin"></i>
                                        {{ __('admin_menu.save') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
