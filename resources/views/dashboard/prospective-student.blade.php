@extends('layouts.admin')

@section('content')
    <section class="section">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-5">
                                <h4>{{ __('admin_menu.prospective_student') }}</h4>
                            </div>
                            <div class="col-7 text-right">
                                <div class="row justify-content-end">
                                    <div class="ml-auto p-2">
                                        <button type="button" id="btnRefresh" class="btn btn-info btn-sm text-right">
                                            {{ __('admin_menu.refresh') }}
                                        </button>
                                        <button type="button" id="btnExport" class="btn btn-primary btn-sm text-right">
                                            {{ __('admin_menu.Export Excel') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2 py-1">
                        <table class="table table-sm table-striped display compact wrap" id="table_prospective_student">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>{{ __('auth.registrationCode') }}</th>
                                <th>{{ __('auth.fullName') }}</th>
                                <th>{{ __('admin_menu.Majors') }}</th>
                                <th>{{ __('admin_menu.Status') }}</th>
                                <th>{{ __('admin_menu.Action') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                    <!-- Modal details -->
                    <div class="modal fade" id="modalReRegistration" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle2">Data Registrasi Ulang</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{-- alert-success show --}}
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="table-responsive">
                                                    <table class="table table-sm table-striped" id="table_re_registration">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">
                                                                #
                                                            </th>
                                                            <th>Tahap Pengisian</th>
                                                            <th>Status</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                {{--                    modal upload Photo --}}
                                                <div class="modal fade" id="modal_photo" tabindex="-1" role="dialog"
                                                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Upload Foto</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <form id="formDataReRegistration" method="POST" action="{{ route('store.student.photo') }}"
                                                                  enctype="multipart/form-data">
                                                                <div class="modal-body">
                                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <div class="form-group row">
                                                                        <label for="inputName"
                                                                               class="col-md-3 col-form-label">{{ __('Foto') }}</label>
                                                                        <div class="col-md-9">
                                                                            <input required name="file" type="file" class="form-control"
                                                                                   id="inputFile"
                                                                                   placeholder="Foto">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form_img">
                                                                        <img src="" alt="" class="img img-fluid" id="img_photo">
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                                                    </button>
                                                                    <button onclick='submitActionPhoto()' type="submit" id="submitBtn"
                                                                            class="btn btn-success">
                                                                        <i id="fa_spin" class="fas fa-spinner fa-spin"></i>
                                                                        {{ __('admin_menu.save') }}`
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="datatableModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered"
                             role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Data</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{-- alert-success show --}}
                                    <div class="cards">
                                        <div class="card-bodys">
                                            <span class="section-title mt-0">Data Pribadi</span>
                                            <hr>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">{{ __('auth.fullName') }}</label>
                                                    <input type="text"
                                                           id="name" readonly
                                                           class="form-control form-control-sm  "
                                                           name="name" required autofocus>
                                                </div>

                                                <div class="form-group col-md-4 col-6">
                                                    <label for="inputEmail4">{{ __('auth.palacebirtday') }}</label>
                                                    <input type="text" readonly
                                                           id="born_place"
                                                           class="form-control form-control-sm  @error('born_place') is-invalid @enderror"
                                                           name="born_place" value="{{ old('born_place') }}"
                                                           required>
                                                </div>

                                                <div class="form-group col-md-4 col-6">
                                                    <label for="inputEmail4">{{ __('auth.dob') }}</label>
                                                    <input type="text"
                                                           id="dob" readonly
                                                           class="form-control form-control-sm  datepicker @error('dob') is-invalid @enderror"
                                                           name="dob" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">{{ __('auth.nisn') }}</label>
                                                    <input type="number" minlength="10" maxlength="10"
                                                           id="no_nisn" readonly
                                                           class="form-control form-control-sm  @error('no_nisn') is-invalid @enderror"
                                                           name="no_nisn" required autofocus>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">{{ __('auth.originSchool') }}</label>
                                                    <input type="text" readonly
                                                           id="old_school"
                                                           class="form-control form-control-sm  @error('old_school') is-invalid @enderror"
                                                           name="old_school" required>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">{{ __('auth.nik') }}</label>
                                                    <input type="number" minlength="16" maxlength="16" id="no_nik"
                                                           readonly
                                                           class="form-control form-control-sm  @error('no_nik') is-invalid @enderror"
                                                           name="no_nik" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">{{ __('auth.gender') }}</label>
                                                    <input type="text" minlength="16" maxlength="16" id="gender"
                                                           readonly
                                                           class="form-control form-control-sm  @error('gender') is-invalid @enderror"
                                                           name="gender" required>

                                                </div>

                                                <div class="form-group col-md-8">
                                                    <label for="inputEmail4">{{ __('auth.address') }}</label>
                                                    <input type="text" id="address" readonly
                                                           class="form-control form-control-sm  @error('address') is-invalid @enderror"
                                                           name="address" value="{{ old('address') }}" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">{{ __('auth.haveKIP') }}</label>
                                                    <input type="text" minlength="16" maxlength="16" id="hasKIP"
                                                           readonly
                                                           class="form-control form-control-sm  @error('hasKIP') is-invalid @enderror"
                                                           name="hasKIP" required value="{{ old('hasKIP') }}">

                                                </div>

                                                <div class="form-group col-md-8">
                                                    <label for="inputEmail4">{{ __('auth.phoneNumber') }}</label>
                                                    <input type="text" minlength="12" maxlength="13" id="no_phone"
                                                           class="form-control form-control-sm  @error('no_phone') is-invalid @enderror"
                                                           name="no_phone" value="{{ old('no_phone') }}" required>
                                                </div>
                                            </div>

                                            <span class="section-title mt-0">{{ __('auth.reportCard') }}</span>
                                            <hr>
                                            <div class="form-row">
                                                <span class="col-12">Bahasa Indonesia</span>
                                                <input type="hidden" value="Bahasa Indonesia"
                                                       name="report[lesson][]">
                                                @for($i=1; $i <= 5; $i++)
                                                    <div class="form-group col-4 col-sm-2 text-center">
                                                        <label for="inputEmail4" class="">{{ $i }}</label>
                                                        <input
                                                            onchange="calcAverage('bindo_semester_', 'average_bindo')"
                                                            id="{{ 'bindo_semester_'.$i }}" type="text" readonly
                                                            class="formatNumber form-control form-control-sm  number_bindo_{{ $i }} @error('bindo_semester_'.$i) is-invalid @enderror"
                                                            name="{{ 'bindo_semester_'.$i }}"
                                                            value="{{ old('bindo_semester_'.$i) }}" required
                                                            autofocus>
                                                    </div>
                                                @endfor

                                                <div class="form-group col-4 col-sm-2">
                                                    <label for="inputEmail4">{{ __('auth.average') }}</label>
                                                    <input type="text" id="average_bindo"
                                                           readonly
                                                           class="formatNumber form-control form-control-sm  average_1 @error('average_bindo') is-invalid @enderror"
                                                           name="average_bindo" value="{{ old('average_bindo') }}"
                                                           required>
                                                </div>

                                                <span class="col-12">Bahasa Inggris</span>
                                                <input type="hidden" value="Bahasa Inggris" name="report[lesson][]">
                                                @for($i=1; $i <= 5; $i++)
                                                    <div class="form-group col-4 col-sm-2 text-center">
                                                        <label for="inputEmail4"
                                                               class="text-center">{{ $i }}</label>
                                                        <input
                                                            onchange="calcAverage('bing_semester_', 'average_binng')"
                                                            id="{{ 'bing_semester_'.$i }}" type="text" readonly
                                                            class="formatNumber form-control form-control-sm  number_bing_{{ $i }} @error('bing_semester_'.$i) is-invalid @enderror"
                                                            name="{{ 'bing_semester_'.$i }}"
                                                            value="{{ old('bing_semester_'.$i) }}" required
                                                            autofocus>
                                                    </div>
                                                @endfor

                                                <div class="form-group col-4 col-sm-2">
                                                    <label for="inputEmail4">{{ __('auth.average') }}</label>
                                                    <input type="text" id="average_binng"
                                                           readonly
                                                           class="formatNumber form-control form-control-sm  average_2 @error('average_binng') is-invalid @enderror"
                                                           name="average_binng" value="{{ old('average_binng') }}"
                                                           required>
                                                </div>

                                                <span class="col-12">Matematika</span>
                                                <input type="hidden" value="Matematika" name="report[lesson][]">
                                                @for($i=1; $i <= 5; $i++)
                                                    <div class="form-group col-4 col-sm-2 text-center">
                                                        <label for="inputEmail4"
                                                               class="text-center">{{ $i }}</label>
                                                        <input
                                                            onchange="calcAverage('mtk_semester_', 'average_mtk')"
                                                            id="{{ 'mtk_semester_'.$i }}" type="text" readonly
                                                            class="formatNumber form-control form-control-sm  number_math_{{ $i }} @error('mtk_semester_'.$i) is-invalid @enderror"
                                                            name="{{ 'mtk_semester_'.$i }}"
                                                            value="{{ old('mtk_semester_'.$i) }}"
                                                            required
                                                            autofocus>
                                                    </div>
                                                @endfor

                                                <div class="form-group col-4 col-sm-2">
                                                    <label for="inputEmail4">{{ __('auth.average') }}</label>
                                                    <input type="text" id="average_mtk"
                                                           readonly
                                                           class="formatNumber form-control form-control-sm  average_3 @error('average_mtk') is-invalid @enderror"
                                                           name="average_mtk" value="{{ old('average_mtk') }}"
                                                           required>
                                                </div>

                                                <span class="col-12">IPA</span>
                                                <input type="hidden" value="IPA" name="report[lesson][]">
                                                @for($i=1; $i <= 5; $i++)
                                                    <div class="form-group col-4 col-sm-2 text-center">
                                                        <label for="inputEmail4"
                                                               class="text-center ">{{ $i }}</label>
                                                        <input
                                                            onchange="calcAverage('ipa_semester_', 'average_ipa')"
                                                            id="{{ 'ipa_semester_'.$i }}" type="text" readonly
                                                            class="formatNumber form-control form-control-sm  number_ipa_{{ $i }} @error('ipa_semester_'.$i) is-invalid @enderror"
                                                            name="{{ 'ipa_semester_'.$i }}"
                                                            required
                                                            autofocus>
                                                    </div>
                                                @endfor

                                                <div class="form-group col-4 col-sm-2">
                                                    <label for="inputEmail4">{{ __('auth.average') }}</label>
                                                    <input type="text" id="average_ipa"
                                                           readonly
                                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                                           name="average_ipa" value="{{ old('average_ipa') }}"
                                                           required>
                                                </div>
                                            </div>

                                            <span class="section-title mt-0">{{ __('admin_menu.Majors') }}</span>
                                            <hr>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="inputEmail4">{{ __('admin_menu.Majors') }}</label>
                                                    <input type="text" id="major_name"
                                                           readonly
                                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                                           name="average_ipa" value="{{ old('average_ipa') }}"
                                                           required>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label
                                                        for="inputEmail4">{{ __('admin_menu.Expertise') }}</label>
                                                    <input type="text" id="expertise_name"
                                                           readonly
                                                           class="formatNumber form-control form-control-sm  average_4 @error('average_ipa') is-invalid @enderror"
                                                           name="average_ipa" value="{{ old('average_ipa') }}"
                                                           required>
                                                </div>
                                            </div>

                                            <span class="section-title mt-0">{{ __('auth.parentData') }}</span>
                                            <hr>
                                            <div class="form-row">
                                                <label class="col-md-12">{{ __('auth.father') }}</label>
                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">{{ __('auth.fullName') }}</label>
                                                    <input type="text" id="name_father" readonly
                                                           class="form-control form-control-sm  @error('name_father') is-invalid @enderror"
                                                           name="name_father" value="{{ old('name_father') }}"
                                                           required
                                                           autofocus>
                                                </div>

                                                <div class="form-group col-md-4 col-6">
                                                    <label for="inputEmail4">{{ __('auth.nik') }}</label>
                                                    <input type="text" id="nik_father" readonly
                                                           class="form-control form-control-sm  @error('nik_father') is-invalid @enderror"
                                                           name="nik_father" value="{{ old('nik_father') }}"
                                                           required>
                                                </div>

                                                <div class="form-group col-md-4 col-6">
                                                    <label for="inputEmail4">{{ __('auth.address') }}</label>
                                                    <input type="text" id="address_father" readonly
                                                           class="form-control form-control-sm  @error('address_father') is-invalid @enderror"
                                                           name="address_father"
                                                           value="{{ old('address_father') }}" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <label class="col-md-12">{{ __('auth.mother') }}</label>
                                                <div class="form-group col-md-4">
                                                    <label for="inputEmail4">{{ __('auth.fullName') }}</label>
                                                    <input type="text" id="name_mother" readonly
                                                           class="form-control form-control-sm  @error('name_mother') is-invalid @enderror"
                                                           name="name_mother" value="{{ old('name_mother') }}"
                                                           required
                                                           autofocus>
                                                </div>

                                                <div class="form-group col-md-4 col-6">
                                                    <label for="inputEmail4">{{ __('auth.nik') }}</label>
                                                    <input type="text" id="nik_mother" readonly
                                                           class="form-control form-control-sm  @error('nik_mother') is-invalid @enderror"
                                                           name="nik_mother" value="{{ old('nik_mother') }}"
                                                           required>
                                                </div>

                                                <div class="form-group col-md-4 col-6">
                                                    <label for="inputEmail4">{{ __('auth.address') }}</label>
                                                    <input type="text" id="address_mother" readonly
                                                           class="form-control form-control-sm  @error('address_mother') is-invalid @enderror"
                                                           name="address_mother"
                                                           value="{{ old('address_mother') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                    </button>
{{--                                    <button type="submit" form="formDataProspecStudent" id="submitBtn"--}}
{{--                                            class="btn btn-success">--}}
{{--                                        <i id="fa_spin" class="fas fa-spinner fa-spin"></i>--}}
{{--                                        {{ __('admin_menu.Re-registration') }}--}}
{{--                                    </button>--}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal Export -->
                    <div class="modal fade" id="modalExport" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle2">Export</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{-- alert-success show --}}
                                    <form id="formExport" method="GET" action="{{ url('/api/export-employee') }}">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row" id="inputDateFirst">
                                                    <label for="inputPosition" class="col-md-12 col-form-label">Tipe Laporan</label>
                                                    <div class="col-md-12">
                                                        <select name="report_type" id="report_type" class="form-control form-control-sm">
                                                            <option value="all">Semua</option>
                                                            <option value="pending">Pending</option>
                                                            <option value="re_registration">Daftar Ulang</option>
                                                            <option value="approve">Diterima</option>
                                                            <option value="reject">Ditolak</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row" id="inputDateFirst">
                                                    <label for="inputPosition" class="col-md-12 col-form-label">Start
                                                        Date</label>
                                                    <div class="col-md-12">
                                                        <input name="start_date" type="text"
                                                               class="form-control form-control-sm form-control-sm datepicker"
                                                               id="inputStartDateExport" placeholder="Start Date">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row" id="inputDateSecond">
                                                    <label for="inputPosition" class="col-md-12 col-form-label">End
                                                        Date</label>
                                                    <div class="col-md-12">
                                                        <input name="end_date" type="text"
                                                               class="form-control form-control-sm form-control-sm datepicker"
                                                               id="inputEndDateExport" placeholder="End Date">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" form="formExport" id="btnExportSubmit"
                                            class="btn btn-primary btn-sm">
                                        <i id="spin_load" class="fa fa-spinner fa-spin" ></i> Export
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>


    {{--modal details--}}
    <div class="modal fade" id="modal_details" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered"
             role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal_details">Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <span id="show_loading" class="text-center">

                </span>
                <div id="content_modal" class="content_modal">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function calcAverage(params, averageInput) {
            var total = 0;
            var arr_semester = [];
            for (var i = 1; i <= 5; i++) {
                var semester = $('#' + params + i).val().replace(',', '');
                var semesterVal
                if (semester && !isNaN(semester)) {
                    semesterVal = parseFloat(semester)
                } else {
                    semesterVal = 0
                }
                arr_semester.push(semesterVal)
                //console.log(isNaN(semester))
                total += parseFloat(semesterVal);
            }
            var average = parseFloat(total) / 5;
            $('#' + averageInput).val(average)
        }

        $(document).ready(function () {
            $('.flatpickr2').flatpickr({
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d",
                maxDate: "@php echo(date('Y')-10).'-12' @endphp",
                minDate: "@php echo(date('Y')-21).'-01' @endphp"
            });

            // initializing Datatable
            var table = $("#table_prospective_student").DataTable({
                "processing": true,
                "serverSide": true,
                "searching": true,
                "ajax": {
                    url: window.app + "/prospective-students",
                    type: "GET",
                },
                language: {
                    paginate: {
                        previous: "<i class='fas fa-angle-left'>",
                        next: "<i class='fas fa-angle-right'>"
                    }
                },
                "dom": 'frt<"row justify-content-sm-between"lip><"clear">',
                "columns": [
                    {
                        data: null,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                        name: "no",
                        width: '5%'
                    },
                    {
                        data: "ppdb_code",
                        name: "ppdb_code",
                        width: '20%'
                    },
                    {
                        data: "name",
                        name: "name",
                        width: '20%'
                    },
                    {
                        data: "major",
                        name: "major",
                        width: '35%'
                    },
                    {
                        data: "status",
                        name: "status",
                        width: '10%'
                    },
                    {
                        data: "action",
                        name: "action",
                        orderable: false,
                        width: '10%'
                    }]
            });

            $('#table_prospective_student tbody').on('click', 'button', function () {
                window.rowData = table.row($(this).parents('tr')).data();
                var id = $(this).data('id')

                if ($(this).data('id-btn') === 'btn-info' + window.rowData.id) {
                    $("#btn_major_add" + window.rowData.id).click(viewTable(window.rowData))
                } else if ($(this).data('id-btn') === 'btn-danger' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(deleteRow(window.rowData.id))
                } else if ($(this).data('id-btn') === 'btn-warning' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(actionRow(window.rowData.id, window.rowData.name, window.rowData.ppdb_code, 'Tolak'))
                } else if ($(this).data('id-btn') === 'btn-success' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(actionRow(window.rowData.id, window.rowData.name, window.rowData.ppdb_code, 'Daftar Ulang'))
                } else if ($(this).data('id-btn') === 'btn_approve' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(actionRow(window.rowData.id, window.rowData.name, window.rowData.ppdb_code, 'Terima'))
                } else if ($(this).data('id-btn') === 'btn-secondary' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(actionRow(window.rowData.id, window.rowData.name, window.rowData.ppdb_code, 'Pending'))
                }else if ($(this).data('id-btn') === 'btn_details' + window.rowData.id) {
                    $("#btn_major_delete" + window.rowData.id).click(openReRegistrationData(window.rowData.id))
                }
            });


            // Edit the data
            function viewTable(rowData) {
                $('#fa_spin').hide()
                $("#alertModal").toggleClass("display-none");
                $("#alertModal").addClass("display-none").removeClass("alert-danger");
                //$("#inputId").val(id);
                $("#datatableModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $("#name").val(rowData.name);
                $("#born_place").val(rowData.born_place);
                $("#dob").val(rowData.dob);
                $("#no_nisn").val(rowData.no_nisn);
                $("#old_school").val(rowData.old_school);
                $("#no_nik").val(rowData.no_nik);
                $("#no_phone").val(rowData.no_phone);
                $("#name_father").val(rowData.name_father);
                $("#address").val(rowData.address);
                $("#nik_father").val(rowData.nik_father);
                $("#address_father").val(rowData.address_father);
                $("#name_mother").val(rowData.name_mother);
                $("#nik_mother").val(rowData.nik_mother);
                $("#major_name").val(rowData.major_name);
                $("#expertise_name").val(rowData.expertise_name);
                $("#address_mother").val(rowData.address_mother);
                $("#gender").val(rowData.gender);
                $("#hasKIP").val(rowData.hasKIP);
                $total_bindo = 0;
                $total_bing = 0;
                $total_mtk = 0;
                $total_ipa = 0;
                for (var i = 1; i <= 5; i++) {
                    var bindo = 'bindo_semester_' + i;
                    var bing = 'bing_semester_' + i;
                    var mtk = 'mtk_semester_' + i;
                    var ipa = 'ipa_semester_' + i;
                    $total_bindo += parseFloat(rowData[bindo]);
                    $total_bing += parseFloat(rowData[bing]);
                    $total_mtk += parseFloat(rowData[mtk]);
                    $total_ipa += parseFloat(rowData[ipa]);

                    $("#bindo_semester_" + i).val(rowData[bindo]);
                    $("#bing_semester_" + i).val(rowData[bing]);
                    $("#mtk_semester_" + i).val(rowData[mtk]);
                    $("#ipa_semester_" + i).val(rowData[ipa]);
                }
                $("#average_ipa").val($total_ipa / 5);
                $("#average_mtk").val($total_mtk / 5);
                $("#average_bindo").val($total_bing / 5);
                $("#average_binng").val($total_bing / 5);
            }

            // Delete row in datatable
            function deleteRow(id) {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: window.app + '/prospective-students/' + id,
                            type: 'DELETE',
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.error) {
                                    $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                                    $("#alertMsgHome").html(data.msg);
                                    setTimeout(function () {
                                        $("#alertHome").addClass("display-none").removeClass("alert-success")
                                    }, 3000);
                                } else {
                                    swalWithBootstrapButtons.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    )
                                    $("#table_prospective_student").DataTable().ajax.reload();
                                }
                            }
                        });
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                })
            }

            function actionRow(id, name, ppdb_code, status) {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Anda yakin?',
                    text: "Calon siswa: "+name+"("+ppdb_code+") akan di ubah statusnya menjadi " + status,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, yakin!',
                    cancelButtonText: 'Tidak, batal!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: window.app + '/prospective-students/' + id,
                            type: 'PATCH',
                            data: {
                                status: status
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                if (data.error) {
                                    Swal.fire(
                                        'Perhatian!',
                                        data.message,
                                        'warning'
                                    )
                                } else {
                                    swalWithBootstrapButtons.fire(
                                        'Berhasil!',
                                        'Status calon siswa telah diubah menjadi ' + status,
                                        'success'
                                    )
                                    $("#table_prospective_student").DataTable().ajax.reload();
                                }
                            },
                            error: function (request, textStatus, errorThrown) {
                                swalWithBootstrapButtons.fire({
                                    title: 'Error!',
                                    text:  errorThrown + " / " + textStatus,
                                    icon: 'error',
                                })
                            }
                        });
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Process cancelled!',
                            'error'
                        )
                    }
                })
            }

            // Display modal for add new data
            $("#btnNewData").click(function (e) {
                e.preventDefault();
                $('#fa_spin').hide()
                $("#alertModal").addClass("display-none").removeClass("alert-danger")
                $("#inputId").val(null)
                $("#datatableModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $('#selectParams').on('change', function () {
                console.log(this.value)
                if (this.value === 'age' || this.value === 'salary') {
                    $('#inputDateFirst').hide();
                    $('#inputDateSecond').hide();
                    $('#inputNumFirst').show();
                    $('#inputNumSecond').show();
                } else if (this.value === 'salary') {
                    $('#inputDateFirst').hide();
                    $('#inputDateSecond').hide();
                    $('#inputNumFirst').show();
                    $('#inputNumSecond').show();
                } else if (this.value === 'start_work') {
                    $('#inputNumFirst').hide();
                    $('#inputNumSecond').hide();
                    $('#inputDateFirst').show();
                    $('#inputDateSecond').show();
                }
            });

            // Display modal for add new data
            $("#btnExport").click(function (e) {
                e.preventDefault();
                $('#spin_load').hide()

                $("#modalExport").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            // submit export
            $("#formExport").submit(function (e) {
                $('#spin_load').show()
                e.preventDefault();
                var selectVal = $("#selectParams option:selected").val();
                var startDate = $("#inputStartDateExport").val();
                var report_type = $("#report_type").val();
                var endDate = $("#inputEndDateExport").val();
                var startNum = $("#inputStartNum").val();
                var endNum = $("#inputEndNum").val();
                $.ajax({
                    url: window.app + '/prospective-students-export',
                    method: 'GET',
                    dataType: 'JSON',
                    data: {
                        select: selectVal,
                        startDate: startDate,
                        endDate: endDate,
                        startNum: startNum,
                        report_type: report_type,
                        endNum: endNum,
                    },
                    success: function (result) {
                        $('#spin_load').hide()
                        window.location = result.url
                    },
                    error: function (request, textStatus, errorThrown) {
                        $('#spin_load').hide()
                        console.log("failed: " + textStatus + " / " + errorThrown);
                    }
                })
            });

            $("#btnRefresh").click(function (e) {
                e.preventDefault();
                $("#table_prospective_student").DataTable().ajax.reload();
            });

            // When submit the form
            $('#formDataMajor').submit(function (e) {
                e.preventDefault()
                var form = $(this),
                    url = form.attr("action")
                var formData = form.serialize();
                $('#fa_spin').show()
                $.ajax({
                    url: window.app + "/prospective-students",
                    data: formData,
                    dataType: 'json', // data type
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                        $('#fa_spin').hide()
                        if (data.error) {
                            $("#alertModal").toggleClass("display-none").toggleClass("alert-danger");
                            $("#alertMessage").html(data.msg);
                            setTimeout(function () {
                                $("#alertModal").addClass("display-none").removeClass("alert-danger")
                            }, 3000);
                        } else {
                            $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                            setTimeout(function () {
                                $("#alertHome").addClass("display-none").removeClass("alert-success")
                            }, 3000);
                            $("#alertMsgHome").html(data.msg);
                            $("#datatableModal").modal("hide");
                            $("#table_prospective_student").DataTable().ajax.reload();
                        }
                    }
                });
            });

            // Reset form when close
            // $('#datatableModal').on('hidden.bs.modal', function () {
            //     $('#datatableModal form')[0].reset();
            // });

            function deleteItem(id) {

            };
        });

        $('#modal_details').on('hidden.bs.modal', function () {
            $("#content_modal").empty();
            $("#form_details").remove();
        })

        function openReRegistrationData(id) {
            $('#table_re_registration').DataTable().destroy()

            $("#modalReRegistration").modal({
                backdrop: 'static',
                keyboard: false
            });
            $("#content_modal").empty();
            $("#form_details").remove();
            var table = $("#table_re_registration").DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "searching": false,
                "dom": 'rt',
                "ajax": {
                    url: window.app + "/re-registration/"+id,
                    type: "GET",
                },
                language: {
                    paginate: {
                        previous: "<i class='fas fa-angle-left'>",
                        next: "<i class='fas fa-angle-right'>"
                    }
                },
                "columns": [
                    {
                        data: null,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        orderable: false,
                        name: "no"
                    },
                    {
                        data: "step_desc",
                        name: "step_desc"
                    },
                    {
                        data: "status",
                        name: "status"
                    },
                    {
                        data: "action",
                        name: "action",
                    }]
            });


            function showDetails(type, id) {
                var url = '';
                switch (type) {
                    case 'details':
                        url = window.app + "/student-details/"+id;
                        break;
                    case 'help':
                        url = window.app + "/student-data-help/"+id;
                        break;
                    case 'parent':
                        url = window.app + "/student-data-parent/"+id;
                        break;
                    case 'score':
                        url = window.app + "/student-score/"+id;
                        break;
                    case 'files':
                        url = window.app + "/student-files/"+id;
                        break;
                }
                $("#content_modal").empty();
                $("#form_details").remove();
                $(".content_modal_inside").remove();
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        $('#show_loading').empty()
                        $(".content_modal_inside").remove();
                        var div_inside = '<div class="content_modal_inside"></div>'
                        $("#content_modal").prepend(div_inside)
                        $(".content_modal_inside").prepend(data)

                        var myInput = document.querySelector(".flatpickr2");
                        if(myInput !== null) {
                            var fp = flatpickr(myInput, {
                                altInput: true,
                                altFormat: "F j, Y",
                                dateFormat: "Y-m-d",
                                defaultDate: "@php echo(date('Y')-16).'-12-31' @endphp",
                                maxDate: "@php echo(date('Y')-21).'-01' @endphp"
                            })

                            var myInput2 = document.querySelector(".flatpickr3");
                            var fp2 = flatpickr(myInput2, {
                                altInput: true,
                                altFormat: "F j, Y",
                                dateFormat: "Y-m-d",
                                defaultDate: "@php echo(date('Y')-16).'-12-31' @endphp",
                                maxDate: "@php echo(date('Y')-21).'-01' @endphp"
                            })

                            var myInput3 = document.querySelector(".flatpickr4");
                            var fp3 = flatpickr(myInput3, {
                                altInput: true,
                                altFormat: "F j, Y",
                                dateFormat: "Y-m-d",
                                defaultDate: "@php echo(date('Y')-16).'-12-31' @endphp",
                                maxDate: "@php echo(date('Y')-21).'-01' @endphp"
                            })

                            fp.setDate($('#father_dob').val(), true)
                            fp2.setDate($('#mother_dob').val(), true)
                            fp3.setDate($('#guardian_parent_dob').val(), true)
                        }

                    },
                });
            }

            function showPhoto(id) {
                $("#content_modal").empty();
                $("#form_details").remove();
                $.ajax({
                    url: window.app + "/student-photo/"+id,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.src !== undefined) {
                            $('#img_photo').attr('src', data.src)
                        }
                    }
                });
            }

            function showModal(rowData, type) {
                $('#show_loading').empty()
                if (type === 'photo') {
                    showPhoto(rowData.id);
                    $("#modal_photo").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $("#submitBtn").text('Simpan')
                } else if (type === 'details' || type === 'help' || type === 'parent' || type === 'score' || type === 'files') {
                    $('#show_loading').append('<i id="spin_load" class="p-3 fa fa-spinner fa-4x fa-spin" ></i>');
                    showDetails(type, rowData.id)
                    $("#modal_details").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            }

            $('#table_re_registration tbody').on('click', 'button', function () {
                window.rowDataDetail = table.row($(this).parents('tr')).data();

                var id = $(this).data('id')
                switch ($(this).data('id-btn')) {
                    case 'btn_photo':
                        $("#btn_photo" + window.rowData.id).click(showModal(window.rowData, 'photo'));
                        break;
                    case 'btn_details':
                        $("#btn_details" + window.rowData.id).click(showModal(window.rowData, 'details'));
                        break;
                    case 'btn_help':
                        $("#btn_help" + window.rowData.id).click(showModal(window.rowData, 'help'));
                        break;
                    case 'btn_parent':
                        $("#btn_parent" + window.rowData.id).click(showModal(window.rowData, 'parent'));
                        break;
                    case 'btn_score':
                        $("#btn_score" + window.rowData.id).click(showModal(window.rowData, 'score'));
                        break;
                    case 'btn_file':
                        $("#btn_file" + window.rowData.id).click(showModal(window.rowData, 'files'));
                        break;
                }
            });
        }

    </script>
@endsection
