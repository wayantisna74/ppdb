<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ public_path('css/report_style.css') }}" rel="stylesheet">
    <style type="text/css">
        .simsun {
            font-family: 'simsun';
        }
    </style>
</head>
<body>
<div class="app">
    <table class="table table-borderless">
        <thead>
        <tr>
            <th width="10%">
                <img style="width: 90px" src="{{ public_path('imgs/dokcapil.png') }}" alt="">
            </th>
            <th width="90%">
                <h5 class="text-center">
                    PEMERINTAH PROVINSI SULAWESI TENGAH <br>
                    DINAS PENDIDIKAN DAN KEBUDAYAAN <br>
                    CABANG DINAS PENDIDIKAN MENENGAH WILAYAH V <br>
                </h5>
                <h4 class="text-center">
                    SMK NEGERI 2 TOILI BARAT <br>
                </h4>
                <h6 class="text-center">
                    Alamat : Jln Pariwisata No. 1 Kec. Toili Barat Kab. Banggai 94765 <br>
                </h6>
            </th>
        </tr>
        </thead>
    </table>
    <hr style="border: 2px solid #000">
    <h5  class="text-center">KARTU BUKTI PENDAFTARAN (KBP)</h5>
    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <td width="30%">{{ __('auth.registrationCode') }}</td>
            <td>{{ $student->ppdb_code }}</td>
        </tr>
        <tr>
            <td width="30%">{{ __('auth.nisn') }}</td>
            <td>{{ $student->no_nisn }}</td>
        </tr>
        <tr>
            <td width="30%">{{ __('auth.fullName') }}</td>
            <td>{{ $student->name }}</td>
        </tr>
        <tr>
            <td width="30%">{{ __('admin_menu.Majors') }}</td>
            <td>{{ $student->major->name .'/'.$student->expertise->name }}</td>
        </tr>
        <tr>
            <td width="30%">{{ __('auth.palacedob') }}</td>
            <td>{{ $student->born_place.' / '.$student->dob }}</td>
        </tr>
        <tr>
            <td width="30%">{{ __('auth.Date Register') }}</td>
            <td>{{ $student->created_at }}</td>
        </tr>
        </tbody>
    </table>

    <table class="table table-borderless table-striped" style="width: 30%; float: right">
        <tbody>
        <tr>
            <td>Paraf Panitia</td>
            <td>
                <img width="100px" src="{{ public_path('imgs/square.jpg') }}" alt="">
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
