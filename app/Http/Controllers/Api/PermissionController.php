<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function Permission()
    {
        $admin_permission = Permission::where('slug', 'create-alumni')->first();
        $su_permission = Permission::where('slug', 'edit-users')->first();

        //RoleTableSeeder.php
        $admin_role = new Role();
        $admin_role->slug = 'admin';
        $admin_role->name = 'Admin';
        $admin_role->save();
        $admin_role->permissions()->attach($admin_permission);

        $su_role = new Role();
        $su_role->slug = 'superuser';
        $su_role->name = 'Super User';
        $su_role->save();
        $su_role->permissions()->attach($su_permission);

        $admin_role = Role::where('slug', 'admin')->first();
        $su_role = Role::where('slug', 'superuser')->first();

        $createTasks = new Permission();
        $createTasks->slug = 'create-alumni';
        $createTasks->name = 'Create Alumni';
        $createTasks->save();
        $createTasks->roles()->attach($admin_role);

        $editUsers = new Permission();
        $editUsers->slug = 'edit-users';
        $editUsers->name = 'Edit Users';
        $editUsers->save();
        $editUsers->roles()->attach($su_role);

        $admin_role = Role::where('slug', 'admin')->first();
        $su_role = Role::where('slug', 'superuser')->first();
        $admin_perm = Permission::where('slug', 'create-alumni')->first();
        $su_perm = Permission::where('slug', 'edit-users')->first();

        $developer = new User();
        $developer->name = 'Admin';
        $developer->username = 'admin';
        $developer->email = 'admin@smkn1toilibarat.sch.id';
        $developer->password = bcrypt('Smkn2tbadmin');
        $developer->save();
        $developer->roles()->attach($admin_role);
        $developer->permissions()->attach($admin_perm);

        $manager = new User();
        $manager->name = 'Super Admin';
        $manager->username = 'manager';
        $manager->email = 'superadmin@smkn1toilibarat.sch.id';
        $manager->password = bcrypt('Smkn2tbsuperuser');
        $manager->save();
        $manager->roles()->attach($su_role);
        $manager->permissions()->attach($su_perm);


        return redirect()->back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        //
    }
}
