<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            "username" => "required|string",
            "password" => "required|string"
        ]);
        if ($validator->fails()) {
            return response()->json([
                "error" => $validator->errors(),
                "message" => "The given data was invalid",
            ], 422);
        } else {
            try {
                if ($this->checkExistUser($request->username)) {
                    if ($user = $this->checkActiveUser($request->username)) {
                        $credentials = ['username' => $request->username, 'password' => $request->password];
                        try {
                            if (!$token = JWTAuth::attempt($credentials)) {
                                return response()->json([
                                    "error" => true,
                                    'message' => 'Invalid login credential'
                                ], 401);
                            }
                        } catch (JWTException $e) {
                            return response()->json([
                                "error" => true,
                                'message' => $e->getMessage()
                            ], 500);
                        }
                        //$user = $request->user();
                        $user = $this->getUser($request->U_UserCode);
                        //$this->insertSession($request, JWTAuth::setToken($token)->getPayload());
                        return response()->json([
                            "success" => true,
                            "error" => false,
                            "token" => $token,
                            "user" => $user
                        ]);
                    } else {
                        return response()->json([
                            "error" => true,
                            "message" => "Your account is not active, please contact the Administrator!"
                        ], 401);
                    }
                } else {
                    return response()->json([
                        "error" => true,
                        "message" => "User Code does not exists!"
                    ], 401);
                }
            } catch (\Exception $exception) {
                return response()->json([
                    "error" => true,
                    "message" => $exception->getMessage(),
                    "trace" => $exception->getTrace()
                ], 401);
            }
        }
    }

    /**
     * @param $username
     * @return bool
     */
    protected function checkExistUser($username): bool
    {
        $user = User::where("username", "=", $username)
            ->first();
        if (is_null($user)) {
            return false;
        }
        return true;
    }

    /**
     * @param $username
     * @return false
     */
    protected function checkActiveUser($username)
    {
        $user = User::where("username", "=", $username)
            ->where("is_active", "=", "Y")
            ->first();
        if (is_null($user)) {
            return false;
        }
        return $user;
    }

    /**
     * @param $username
     * @return mixed
     */
    protected function getUser($username)
    {
        return User::where('username', $username)
            ->get();
    }

    public function authUser(Request $request)
    {
        return $request->user();
    }
}
