<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Major;
use App\Models\Role;
use App\Models\Student;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $columns = $request->columns;
        $order = $request->order;
        $search = $request->search;
        $search = $search['value'];

        $col = '';
        $dir = "";

        $valid_columns = [];

        for ($i = 0; $i < count($columns); $i++) {
            if ($columns[$i]['data'] != "action" && $columns[$i]['data'] != "no") {
                $valid_columns[] = $columns[$i]['data'];
            }
            if (!empty($order)) {
                if ($order[0]['column'] == $i) {
                    $col = $columns[$i]['data'];
                    $dir = $order[0]['dir'];
                }
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        $query = User::select('*');

        if ($order != null && $col != 'no') {
            if ($col== 'role') {
                $query->orderBy('role_id', $dir);
            } else {
                $query->orderBy($col, $dir);
            }
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $term) {
                if ($term != 'role') {
                    if ($x == 0) {
                        $query->where($term, "LIKE", "%$search%");
                    } else {
                        $query->orWhere($term, "LIKE", "%$search%");
                    }
                }
                $x++;
            }
        }
        $query->offset($start)->limit($length);
        $datatable = $query->get();
        $data = [];
        $edit = __('admin_menu.edit');
        $delete = __('admin_menu.delete');
        foreach ($datatable as $index => $rows) {
            $id = $rows->id;
            $data[] = [
                "no" => $index+1,
                "id" => $rows->id,
                "name" => $rows->name,
                "username" => $rows->name,
                "role" => $rows->role->name,
                "role_id" => $rows->role_id,
                "email" => $rows->email,
                "is_active" => $rows->is_active,
                "action" => '<button id="btn_major_add'.$rows->id.'" data-id-btn="btn-warning' . $id . '"
                                class="btn btn-sm btn-warning mr-1">'.$edit.'</button>
                             <button  id="btn_major_delete'.$rows->id.'" data-id-btn="btn-danger' . $id . '"
                                class="btn btn-sm btn-danger mr-1">'.$delete.'</button>'
            ];
        }

        $total_data = $this->totalData();
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        return response()->json($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $id = $request->id;
            if ($id) {
                $this->update($request, $id);

                return response()->json([
                    "error" => false,
                    "msg" => "Data updated!"
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                   'name' => 'required',
                   'username' => 'required|unique:users,username',
                   'email' => 'required|email|unique:users,email',
                   'password' => 'required|min:8'
                ]);
                if ($validator->fails()) {
                    return response()->json([
                       "error" => true,
                       "msg" => $validator->getMessageBag()
                    ]);
                }
                $form_data = new User();
                $form_data->name = $request->name;
                $form_data->username = Str::slug($request->username);
                $form_data->email = $request->email;
                $form_data->role_id = $request->role_id;
                $form_data->is_active = $request->is_active;
                $form_data->password = bcrypt($request->password);
                $form_data->save();

                return response()->json([
                    "error" => false,
                    "msg" => "Data saved!"
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                "error" => true,
                "msg" => $e->getMessage()
            ]);
        }
    }

    public function getRoles()
    {
        return response()->json(Role::all());
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $data_form = Role::where("id", "=", $id)
            ->selectRaw("
                id
                ,name
                ,guard_name
            ")
            ->first();

        return response()->json([
            "row" => $data_form
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $form_data = User::where("id", "=", $id)->first();
        if ($form_data) {
            $form_data->name = $request->name;
            $form_data->username = Str::slug($request->username);
            $form_data->email = $request->email;
            $form_data->role_id = $request->role_id;
            $form_data->is_active = $request->is_active;
            $form_data->save();
        }
    }

    public function changePassword(Request $request, $id)
    {
        $form_data = User::where("id", "=", $id)->first();
        if ($form_data) {
            $form_data->password = bcrypt($request->password);
            $form_data->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            Role::where("id", "=", $id)->delete();
            return response()->json([
                "error" => false,
                "msg" => "Data deleted successfuly!"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "error" => true,
                "msg" => $e->getMessage()
            ]);
        }
    }

    /**
     * @return mixed
     */
    private function totalData()
    {
        return Role::count();
    }
}
