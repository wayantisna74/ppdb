<?php

namespace App\Http\Controllers;

use App\Helper\PageData;
use App\Models\Student;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use PageData;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $students = Student::where("approval_step", "<>", "A")
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $pending = Student::where("approval_step", "=", "P")->count();
        $reject = Student::where("approval_step", "=", "R")->count();
        $reregistration = Student::where("approval_step", "=", "G")->count();

        return view('dashboard.index', [
            'students' => $students,
            'pending' => $pending,
            'reject' => $reject,
            'reregistration' => $reregistration,
        ]);
    }

    /**
     * @param $page_type
     * @param $query
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|void
     */
    public function page($page_type, $query)
    {
        if ($page_type && $query) {
            if ($page_type == 'registration') {
                switch ($query) {
                    case 'prospective-students':
                        return view('dashboard.prospective-student');
                        break;
                    case 're-registration':
                        return view('dashboard.re-registration');
                        break;
                    default:
                        return abort(404);
                        break;
                }
            } elseif ($page_type == 'settings') {
                switch ($query) {
                    case 'permissions':
                        return view('dashboard.permission');
                        break;
                    case 'roles':
                        return view('dashboard.roles');
                        break;
                    case 'student-registration':
                        return view('dashboard.ppdb');
                        break;
                    case 'school':
                        return view('dashboard.school', [
                            'school' => $this->school()
                        ]);
                        break;
                    case 'users':
                        return view('dashboard.user');
                        break;
                    case 'alumni':
                        return view('dashboard.alumni');
                        break;
                    case 'majors':
                        return view('dashboard.majors');
                        break;
                    case 'expertise':
                        return view('dashboard.expertise', [
                            'majors' => $this->majors()
                        ]);
                        break;
                    default:
                        return abort(404);
                        break;
                }
            } elseif ($page_type == 'page') {
                switch ($query) {
                    case 'students':
                        return view('dashboard.student');
                        break;
                    case 'alumni':
                        return view('dashboard.alumni');
                        break;
                    default:
                        return abort(404);
                        break;
                }
            }
        } else {
            return redirect()->route('home');
        }
    }
}
