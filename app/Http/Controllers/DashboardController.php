<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $students = Student::where("approval_step", "<>", "A")
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $pending = Student::where("approval_step", "=", "P")->distinct()->count();
        $reject = Student::where("approval_step", "=", "R")->distinct()->count();
        $reregistration = Student::where("approval_step", "=", "G")->distinct()->count();

        return view('dashboard.index', [
            'students' => $students,
            'pending' => $pending,
            'reject' => $reject,
            'reregistration' => $reregistration,
        ]);
    }
}
