<?php

namespace App\Http\Controllers;

use App\Helper\PageData;
use App\Models\HomeData;
use App\Models\ListSchool;
use App\Models\PPDB;
use App\Models\Student;
use App\Models\TimeLine;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    use PageData;
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home()
    {
        return view('index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('welcome', [
            'timeline' => collect($this->timeline())
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function mainLogo(): \Illuminate\Http\JsonResponse
    {
        return response()->json(asset('imgs/web.png'));
    }

    /**
     * @return \string[][]
     */
    public function timeline()
    {
        return [
            [
                'date' => '15 mei s/d 3 Juni 2020',
                'icon' => 'fas fa-file-alt',
                'text' => 'Pengisian Formulir Online'
            ],
            [
                'date' => '10 s/d 17 Juni 2020',
                'icon' => 'fas fa-check-square',
                'text' => 'Seleksi Calon siswa baru'
            ],
            [
                'date' => '25 Juni 2020',
                'icon' => 'fas fa-envelope',
                'text' => 'Pengumuman Seleksi'
            ],
            [
                'date' => '26 s/d 29 Juni 2020',
                'icon' => 'fas fa-file-signature',
                'text' => 'Registrasi Online ( yang LULUS )'
            ],
            [
                'date' => '8 s/10 Juli 2020',
                'icon' => 'fas fa-user-friends',
                'text' => 'Pengenalan Lingkungan Sekolah'
            ],
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function homeData(Request $request): \Illuminate\Http\JsonResponse
    {
        $lesson_data = [];
        for ($j = 1; $j <= 5; $j++) {
            $semester = 'semester_' . $j;
            $i = 1;
            $indexRow = $i;
            $lesson = [
                'lesson_bindo' . $j => '',
                'lesson_bing' . $j => '',
                'lesson_mtk' . $j => '',
                'lesson_ipa' . $j => '',
            ];
            $lesson_data = array_merge($lesson_data, $lesson);
        }

        $data = array_merge([
            "ppdb_code" => '',
            "id" => '',
            "name" => null,
            "born_place" => '',
            "old_school" => '',
            "no_nisn" => '',
            "no_nik" => '',
            "dob" => '',
            "gender" => '',
            "address" => '',
            "major_id" => '',
            "expertise_id" => '',
            "hasKIP" => '',
            "no_phone" => '',
            "nik_father" => '',
            "address_father" => '',
            "name_father" => '',
            "name_mother" => '',
            "nik_mother" => '',
            "address_mother" => '',
            "major_name" => '',
            "expertise_name" => '',
            "major" => '',
            "parentCheck" => '',
            "average_ipa" => '',
            "average_mtk" => '',
            "average_bing" => '',
            "average_bindo" => '',
        ], $lesson_data);

        $min_date = (date('Y') - 21) . '-01';

        $list_school = ListSchool::select('name')->distinct()->get();

        $ppdb_now = $this->ppdbThisYear();

        return response()->json([
            'form' => $data,
            'min_date' => $min_date,
            'list_school' => $list_school,
            "ppdb" => $ppdb_now,
            'date_now' => date('Y-m-d')
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function welcomeData(Request $request): \Illuminate\Http\JsonResponse
    {
        $ppdb = PPDB::where("start_year", "=", date('Y'))->first();
        $home_data = HomeData::all();
        $time_lines = TimeLine::all();
        return response()->json([
            'timelines' => $time_lines,
            'home_data' => $home_data
        ]);
    }
}
