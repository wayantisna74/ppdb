<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudentDetails extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_bird_card' => 'required',
            'religion_id' => 'required',
            'special_need_id' => 'required',
            'nationality' => 'required',
            'province_id' => 'required',
            'regency_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
            'dusun_name' => 'required',
            'rt_name' => 'required',
            'rw_name' => 'required',
            'zip_code' => 'required',
            'residence_id' => 'required',
            'transportation_id' => 'required',
            'family_order' => 'required',
            'sibling_number' => 'required',
            'blood_group_id' => 'required',
            'home_phone' => 'required',
            'email' => 'required',
            'extracurricular_id' => 'required',
            'height' => 'required',
            'head_circumference' => 'required',
            'weight' => 'required',
            'school_home_distance' => 'required',
            'travel_time' => 'required',
        ];
    }
}
