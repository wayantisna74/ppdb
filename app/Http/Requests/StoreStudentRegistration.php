<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudentRegistration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'name_father' => 'required',
            'name_mother' => 'required',
            'born_place' => 'required|string',
            'dob' => 'required|date',
            'no_nisn' => 'required|max:10|min:10|regex:/^[0-9 ]+$/|unique:students,no_nisn',
            'expertise_id' => 'required',
            'major_id' => 'required',
            'nik_father' => 'required',
            'nik_mother' => 'required',
            'old_school' => 'required|string',
            'no_nik' => 'required|max:16|min:16|regex:/^([0-9\s+\-\+\(\)]*)$/|unique:students,no_nik',
            'gender' => 'required|string',
            'address' => 'required|string',
            'address_father' => 'required',
            'address_mother' => 'required',
            'hasKIP' => 'required',
            'agree_tos' => 'required',
            'parentCheck' => 'required',
            'no_phone' => 'required|phone:ID',
            'average_ipa' => 'required|numeric|lte:100',
            'average_mtk' => 'required|numeric|lte:100',
            'average_bing' => 'required|numeric|lte:100',
            'average_bindo' => 'required|numeric|lte:100',
        ];

        for ($i=1; $i <= 5; $i++) {
            $rules = array_merge($rules, [
                'lesson_bindo'.$i => 'required|numeric|lte:100',
                'lesson_bing'.$i => 'required|numeric|lte:100',
                'lesson_mtk'.$i => 'required|numeric|lte:100',
                'lesson_ipa'.$i => 'required|numeric|lte:100',
            ]);
        }
        return $rules;
    }
}
