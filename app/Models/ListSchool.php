<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ListSchool extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'list_schools';

    public function major()
    {
        return $this->belongsTo(Major::class, 'major_id');
    }
}
