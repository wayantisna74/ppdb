<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KipWorthyReason extends Model
{
    protected $table = 'kip_worthy_reason';
}
