<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PermissionUser extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public function permission()
    {
        return $this->hasOne(Permission::class, 'id', 'permission_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'id', 'permission_id');
    }
}
