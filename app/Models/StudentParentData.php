<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentParentData extends Model
{
    protected $table = 'students';

    protected $fillable = [
        'name_father',
        'nik_father',
        'name_mother',
        'nik_mother'
    ];
}
