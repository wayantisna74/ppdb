<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Major extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'majors';

    public function expertise()
    {
        return $this->hasMany(Expertise::class, 'major_id');
    }
}
