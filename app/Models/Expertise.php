<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Expertise extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'expertise';

    public function major()
    {
        return $this->belongsTo(Major::class, 'major_id');
    }
}
