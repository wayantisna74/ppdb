module.exports = {
	globDirectory: 'public/',
	globPatterns: [
		'**/*.{js,css,eot,svg,ttf,woff,woff2,png,jpg,md,json,swf,otf,html,ico}'
	],
	ignoreURLParametersMatching: [
		/^utm_/,
		/^fbclid$/
	],
	swDest: 'public/sw.js'
};