const mix = require('laravel-mix');
const path = require('path')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// require('laravel-mix-workbox');
const zlib = require("zlib");
const CompressionPlugin = require("compression-webpack-plugin");

mix.js('resources/js/app.js', 'public/js')
  .vue({version: 2});
mix.extract(['vue', 'vuex', 'axios'], 'vendor1.js');
mix.extract(['vuetify'], 'vendor2.js');
mix.sass('resources/sass/style.scss', 'public/css')
  .sourceMaps()
  .version();

// mix.generateSW({
//   exclude: [/\.(?:png|jpg|jpeg|svg)$/],
//   runtimeCaching: [
//     {
//       urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
//       handler: 'CacheFirst',
//       options: {
//         cacheName: 'images',
//         expiration: {
//           maxEntries: 10,
//         },
//       },
//     },
//     {
//       handler: 'StaleWhileRevalidate',
//       urlPattern: /\.(?:js|css|html)$/,
//       options: {
//         cacheName: 'static-assets-cache',
//         cacheableResponse: {
//           statuses: [0, 200]
//         },
//         expiration: {
//           maxEntries: 100,
//           maxAgeSeconds: 24 * 60 * 60 * 60
//         }
//       }
//     },
//   ],
//   clientsClaim: true,
//   skipWaiting: true,
//   maximumFileSizeToCacheInBytes: 3872864
// });

// mix.setResourceRoot(process.env.ROUTE_BASE);

let preprocessorsExcludes = [];
if (mix.preprocessors) {
  mix.preprocessors.forEach(preprocessor => {
    preprocessorsExcludes.push(preprocessor.test());
  });
}

mix.webpackConfig({
  output: {
    publicPath: process.env.MIX_BASE_PATH,
    filename: 'assets/[name].js',
    //chunkFilename: 'assets/[name].js'
    chunkFilename: 'assets/[name].js?id=[chunkhash]'
  },

  optimization: {
    minimize: true,
    splitChunks: {
      chunks: 'all'
    }
  },

  plugins: [
    new CompressionPlugin({
      filename: "[path][base].gz",
      algorithm: "gzip",
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
    new CompressionPlugin({
      filename: "[path][base].br",
      algorithm: "brotliCompress",
      test: /\.(js|css|html|svg)$/,
      compressionOptions: {
        params: {
          [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
        },
      },
      threshold: 10240,
      minRatio: 0.8,
    }),

    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      reportFilename: path.join(`${__dirname}/public`, 'webpack-report.html'),
      openAnalyzer: false,
      logLevel: 'silent'
    }),

    new VuetifyLoaderPlugin({
      match(originalTag, {kebabTag, camelTag, path, component}) {
        if (kebabTag.startsWith('core-')) {
          return [camelTag, `import ${camelTag} from '@/components/core/${camelTag.substring(4)}.vue'`]
        }
      }
    })
  ],

});
