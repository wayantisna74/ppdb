<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->string("no_bird_card");
            $table->unsignedBigInteger('religion_id');
            $table->unsignedBigInteger('special_need_id');
            $table->string('nationality');
            $table->char('province_id', 2);
            $table->char('regency_id', 4);
            $table->char('district_id', 7);
            $table->char('village_id', 10);
            $table->string('dusun_name');
            $table->string('rt_name');
            $table->string('rw_name');
            $table->string('zip_code', 20);
            $table->unsignedBigInteger('residence_id');
            $table->unsignedBigInteger('transportation_id');
            $table->tinyInteger("family_order");
            $table->unsignedBigInteger('blood_group_id');
            $table->string("home_phone", 20)->nullable();
            $table->string("email", 100)->nullable();
            $table->unsignedBigInteger("extracurricular_id");
            $table->decimal("height", 6, 2);
            $table->decimal("weight", 6, 2);
            $table->decimal("school_home_distance", 6, 2);
            $table->decimal("travel_time", 6, 2);

            // Help
            $table->string("kks_no", 100)->nullable();
            $table->string("is_kps_receiver", 1)->default('N');
            $table->string("kps_no", 100)->nullable();
            $table->string("is_pip_worthy", 1)->default('N');
            $table->string("pip_worthy_reason")->nullable();
            $table->string("pip_no", 100)->nullable();
            $table->string("pip_name", 100)->nullable();
            $table->string("is_kip_receiver", 1)->default('N');
            $table->string("is_kip_physical_receiver", 1)->default('N');

            // Parent
            $table->string("father_born_place", 200)->nullable();
            $table->date("father_dob")->nullable();
            $table->string("father_education")->nullable();
            $table->string("father_job")->nullable();
            $table->string("father_income")->nullable();
            $table->string("father_special_need")->nullable();

            $table->string("mother_born_place", 200)->nullable();
            $table->date("mother_dob")->nullable();
            $table->string("mother_education")->nullable();
            $table->string("mother_job")->nullable();
            $table->string("mother_income")->nullable();
            $table->string("mother_special_need")->nullable();

            $table->string("guardian_parent_born_place", 200)->nullable();
            $table->date("guardian_parent_dob")->nullable();
            $table->string("guardian_parent_education")->nullable();
            $table->string("guardian_parent_job")->nullable();
            $table->string("guardian_parent_income")->nullable();
            $table->string("guardian_parent_special_need")->nullable();

            $table->string("is_details_complete")->default("N");
            $table->string("is_help_complete")->default("N");
            $table->string("is_parent_complete")->default("N");
            $table->timestamps();
        });

        Schema::create('special_needs', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });

        Schema::create('residence', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });

        Schema::create('transportation', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });

        Schema::create('blood_groups', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });

        Schema::create('extracurricular', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });

        Schema::create('kip_worthy_reason', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });

        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });

        Schema::create('income', function (Blueprint $table) {
            $table->id();
            $table->string("value");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });

        Schema::create('parent_jobs', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('active', 1)->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_details');
        //Schema::dropIfExists('files');
        Schema::dropIfExists('special_needs');
        Schema::dropIfExists('residence');
        Schema::dropIfExists('transportation');
        Schema::dropIfExists('blood_groups');
        Schema::dropIfExists('extracurricular');
        Schema::dropIfExists('kip_worthy_reason');
        Schema::dropIfExists('schools');
        Schema::dropIfExists('income');
        Schema::dropIfExists('parent_jobs');
    }
}
