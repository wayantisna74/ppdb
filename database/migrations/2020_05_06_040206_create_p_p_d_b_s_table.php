<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePPDBSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ppdb', function (Blueprint $table) {
            $table->id();
            $table->year("start_year");
            $table->year("end_year");
            $table->date("open_date");
            $table->string("is_open", 1)->default("Y");
            $table->unsignedBigInteger("created_by");
            $table->unsignedBigInteger("updated_by")->nullable();
            $table->timestamps();
        });

        Schema::create("school", function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->text("address");
            $table->string("no_phone")->nullable();
            $table->string("no_zip")->nullable();
            $table->string("name_principle");
            $table->string("nik_principle");
            $table->timestamps();
        });

        Schema::create("majors", function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("slug");
            $table->text("description")->nullable();
            $table->timestamps();
        });

        Schema::create("expertise", function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("slug");
            $table->text("description")->nullable();
            $table->unsignedBigInteger("major_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppdb');
        Schema::dropIfExists('school');
        Schema::dropIfExists('majors');
        Schema::dropIfExists('expertise');
    }
}
