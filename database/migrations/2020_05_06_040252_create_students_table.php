<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger("student_id")->nullable();
            $table->string('username');
            $table->string("is_active", 1)->default('Y');
        });

        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("born_place");
            $table->date("dob");
            $table->string("old_school");
            $table->string("no_nisn");
            $table->string("no_nik");
            $table->string("gender");
            $table->text("address");
            $table->unsignedBigInteger("major_id");
            $table->unsignedBigInteger("expertise_id");
            $table->string("no_phone");
            $table->string("hasKIP", 1)->default("N");
            $table->string("name_father")->nullable();
            $table->string("nik_father")->nullable();
            $table->string("address_father")->nullable();
            $table->string("name_mother")->nullable();
            $table->string("nik_mother")->nullable();
            $table->string("address_mother")->nullable();
            $table->string("agree_tos", 1)->default("N");
            $table->string("is_file_complete", 1)->default("N");
            $table->string("is_active", 1)->default('Y');
            $table->string("is_graduate", 1)->default('N');
            $table->dateTime("date_graduate")->nullable();
            $table->string("approval_step")->default("P");
            $table->unsignedBigInteger("ppdb_id");
            $table->string("ppdb_code");

            $table->timestamps();

            $table->foreign("major_id")->references("id")
                ->on("majors")->onUpdate('cascade');

            $table->foreign("expertise_id")->references("id")
                ->on("expertise")->onUpdate('cascade');

            /**
             * Approval Step
             * P = pending
             * R = Reject
             * G = Re registration
             * A = Approve
             */
        });

        Schema::create('report_cards', function (Blueprint $table) {
            $table->id();
            $table->string("lesson");
            $table->double("semester_1", 20, 4)->default(0);
            $table->double("semester_2", 20, 4)->default(0);
            $table->double("semester_3", 20, 4)->default(0);
            $table->double("semester_4", 20, 4)->default(0);
            $table->double("semester_5", 20, 4)->default(0);
            $table->double("semester_6", 20, 4)->default(0);
            $table->string("type")->default("ppdb");
            $table->unsignedBigInteger("student_id");
            $table->timestamps();

            $table->foreign("student_id")
                ->references("id")->on("students")
                ->onDelete("cascade");
        });

        Schema::create("images", function (Blueprint $table) {
            $table->id();
            $table->string("file_name");
            $table->string("file_slug");
            $table->string("file_path");
            $table->string("file_type");
            $table->unsignedBigInteger("doc_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
        Schema::dropIfExists('report_cards');
        Schema::dropIfExists('images');
    }
}
