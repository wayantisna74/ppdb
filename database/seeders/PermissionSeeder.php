<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'Superuser',
            'Admin',
            'Student',
            'Re-registration',
        ];
        foreach ($permissions as $permission) {
            \App\Models\Role::create([
                'name' => $permission,
                'guard_name' => 'web'
            ]);
        }
    }
}
