<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where("name", "=", "Superuser")->first();
        $user = User::create([
            'name' => 'superuser',
            'username' => 'manager',
            'email' => 'superuser@smkn2toilibarat',
            'password' => bcrypt('SuperUser2211'),
            'role_id' => $role->id
        ]);
    }
}
