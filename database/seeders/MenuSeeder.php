<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\UserMenu;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Menu::create([
            'menu' => 'PPDB',
            'parent_menu' => 0,
            'icon' => 'mdi-school',
            'icon_alt' => 'mdi-menu-down',
            'route_name' => '',
            'model' => 'N',
            'has_child' => 'Y',
            'has_route' => 'N',
            'order_line' => 1,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ]);

        Menu::create([
            'menu' => 'Pengaturan',
            'parent_menu' => 0,
            'icon' => 'mdi-settings',
            'icon_alt' => 'mdi-menu-down',
            'route_name' => '',
            'model' => 'N',
            'has_child' => 'Y',
            'has_route' => 'N',
            'order_line' => 1,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ]);

        Menu::create([
            'menu' => 'Calon Siswa',
            'parent_menu' => 1,
            'icon' => 'mdi-account-circle-outline',
            'icon_alt' => null,
            'route_name' => '/dashboard/prospective-student',
            'model' => 'N',
            'has_child' => 'N',
            'has_route' => 'Y',
            'order_line' => 1,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ]);

        Menu::create([
            'menu' => 'Jurusan',
            'parent_menu' => 2,
            'icon' => 'mdi-school',
            'icon_alt' => null,
            'route_name' => '/dashboard/master/major',
            'model' => 'N',
            'has_child' => 'N',
            'has_route' => 'Y',
            'order_line' => 1,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ]);

        Menu::create([
            'menu' => 'PPDB',
            'parent_menu' => 2,
            'icon' => 'mdi-login-variant',
            'icon_alt' => null,
            'route_name' => '/dashboard/master/ppdb',
            'model' => 'N',
            'has_child' => 'N',
            'has_route' => 'Y',
            'order_line' => 2,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ]);

        Menu::create([
            'menu' => 'Peran',
            'parent_menu' => 2,
            'icon' => 'mdi-account-settings-variant',
            'icon_alt' => null,
            'route_name' => '/dashboard/master/role',
            'model' => 'N',
            'has_child' => 'N',
            'has_route' => 'Y',
            'order_line' => 2,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ]);

        Menu::create([
            'menu' => 'User',
            'parent_menu' => 2,
            'icon' => 'mdi-account-circle',
            'icon_alt' => null,
            'route_name' => '/dashboard/master/user',
            'model' => 'N',
            'has_child' => 'N',
            'has_route' => 'Y',
            'order_line' => 2,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ]);

        Menu::create([
            'menu' => 'Home Data',
            'parent_menu' => 2,
            'icon' => 'mdi-home',
            'icon_alt' => null,
            'route_name' => '/dashboard/master/home-data',
            'model' => 'N',
            'has_child' => 'N',
            'has_route' => 'Y',
            'order_line' => 2,
            'created_by' => 1,
            'created_at' => Carbon::now()
        ]);

        for ($i=1; $i<=8; $i++) {
            UserMenu::create([
                'menu_id' => $i,
                'user_id' => 1,
                'created_at' => Carbon::now()
            ]);

            UserMenu::create([
                'menu_id' => $i,
                'user_id' => 2,
                'created_at' => Carbon::now()
            ]);
        }
    }
}
