INSERT INTO `religions` VALUES
  (NULL, 'Budha', 'Y', NULL, NULL),
  (NULL, 'Hindu', 'Y', NULL, NULL),
  (NULL, 'Islam', 'Y', NULL, NULL),
  (NULL, 'Katholik', 'Y', NULL, NULL),
  (NULL, 'Kristen', 'Y', NULL, NULL);


INSERT INTO `special_needs` VALUES
  (NULL, "Tidak Cacat", "Y", NULL, NULL),
  (NULL, "Tuna Netra", "Y", NULL, NULL),
  (NULL, "Tuna Rungu", "Y", NULL, NULL),
  (NULL, "Grahita Ringan", "Y", NULL, NULL),
  (NULL, "Grahita Sedang ", "Y", NULL, NULL),
  (NULL, "Daksa Ringan", "Y", NULL, NULL),
  (NULL, "Daksa Sedang ", "Y", NULL, NULL),
  (NULL, "Laras", "Y", NULL, NULL),
  (NULL, "Wicara", "Y", NULL, NULL),
  (NULL, "Tuna Ganda", "Y", NULL, NULL),
  (NULL, "Hiper akti", "Y", NULL, NULL),
  (NULL, "Cerdas Istimewa", "Y", NULL, NULL),
  (NULL, "Bakat Istimewa", "Y", NULL, NULL),
  (NULL, "Kesulitan Belajar", "Y", NULL, NULL),
  (NULL, "Narkoba", "Y", NULL, NULL),
  (NULL, "Indigo", "Y", NULL, NULL),
  (NULL, "Down Sindrom", "Y", NULL, NULL),
  (NULL, "Autis", "Y", NULL, NULL);


INSERT INTO `residence` VALUES
    (NULL, "Bersama Orang Tua", "Y", NULL, NULL),
    (NULL, "Wali", "Y", NULL, NULL),
    (NULL, "Kos", "Y", NULL, NULL),
    (NULL, "Asrama", "Y", NULL, NULL),
    (NULL, "Panti Asuhan", "Y", NULL, NULL),
    (NULL, "Pesantren", "Y", NULL, NULL),
    (NULL, "Lainnya", "Y", NULL, NULL);



INSERT INTO `transportation` VALUES
  (NULL, "Jalan Kaki", "Y", NULL, NULL),
    (NULL, "Kendaraan Pribadi", "Y", NULL, NULL),
    (NULL, "Kendaraan Umum/Angkot/Pete-pete", "Y", NULL, NULL),
    (NULL, "Jemputan Sekolah", "Y", NULL, NULL),
    (NULL, "Kereta Api", "Y", NULL, NULL),
    (NULL, "Ojek", "Y", NULL, NULL),
    (NULL, "Andong/Bendi/Sado/Dokar/Delman/Beca", "Y", NULL, NULL),
    (NULL, "Perahu Penyebrangan/Rakit/Getek", "Y", NULL, NULL),
    (NULL, "Lainnya", "Y", NULL, NULL);


INSERT INTO `blood_groups` VALUES
    (NULL, "A", "Y", NULL, NULL),
    (NULL, "B", "Y", NULL, NULL),
    (NULL, "AB", "Y", NULL, NULL),
    (NULL, "O", "Y", NULL, NULL),
    (NULL, "Tidak Tahu", "Y", NULL, NULL);


INSERT INTO `extracurricular` VALUES
    (NULL, "Tidak Ada", "Y", NULL, NULL),
    (NULL, "Wirausaha/Koperasi/Keterampilan Produktif", "Y", NULL, NULL),
    (NULL, "Bahasa", "Y", NULL, NULL),
    (NULL, "Karya Ilmiah Remaja/Sains KIR", "Y", NULL, NULL),
    (NULL, "Kerohanian", "Y", NULL, NULL),
    (NULL, "Komputer dan teknologi", "Y", NULL, NULL),
    (NULL, "Olahraga/Beladiri", "Y", NULL, NULL),
    (NULL, "Otomotif/Bengkel/Bikers", "Y", NULL, NULL),
    (NULL, "Palang Merah Remaja (PMR)", "Y", NULL, NULL),
    (NULL, "Paskibra", "Y", NULL, NULL),
    (NULL, "Palang Keamanan Sekolah (PKS)", "Y", NULL, NULL),
    (NULL, "Pencinta Alam", "Y", NULL, NULL),
    (NULL, "Pramuka", "Y", NULL, NULL),
    (NULL, "Seni Media/Jurnalistik/Fotografi", "Y", NULL, NULL),
    (NULL, "Seni Musik", "Y", NULL, NULL),
    (NULL, "Seni Tari dan Peran", "Y", NULL, NULL),
    (NULL, "Unit Kesehatan Sekolah (UKS)", "Y", NULL, NULL);


INSERT INTO `kip_worthy_reason` VALUES
  (NULL, "Pemegang PKH/KPS/KIP", "Y", NULL, NULL),
    (NULL, "Penerima BSM 2014", "Y", NULL, NULL),
    (NULL, "Yatim Piatu/Panti Asuhan/Panti Sosial", "Y", NULL, NULL),
    (NULL, "Dampat Becana Alam", "Y", NULL, NULL),
    (NULL, "Pernah Drop Out", "Y", NULL, NULL),
    (NULL, "Siswa Miskin/Rentan Miskin", "Y", NULL, NULL),
    (NULL, "Daerah Konflik", "Y", NULL, NULL),
    (NULL, "Keluarga Terpidana", "Y", NULL, NULL),
    (NULL, "Kelainan Fisik", "Y", NULL, NULL);


INSERT INTO `schools` VALUES
  (NULL, "Tidak Sekolah", "Y", NULL, NULL),
    (NULL, "Putus Sekolah", "Y", NULL, NULL),
    (NULL, "Putus SD", "Y", NULL, NULL),
    (NULL, "SD Sederajat", "Y", NULL, NULL),
    (NULL, "SMP Sederajat", "Y", NULL, NULL),
    (NULL, "SMA Sederajat", "Y", NULL, NULL),
    (NULL, "D1", "Y", NULL, NULL),
    (NULL, "D2", "Y", NULL, NULL),
    (NULL, "D3", "Y", NULL, NULL),
    (NULL, "S1", "Y", NULL, NULL),
    (NULL, "S2", "Y", NULL, NULL),
    (NULL, "S3", "Y", NULL, NULL);


INSERT INTO `income` VALUES
  (NULL, "Kurang Dari 500,000", "Y",NULL, NULL),
    (NULL, "500,000 - 999,000","Y",NULL, NULL),
    (NULL, "1000,000 - 1,999,999", "Y", NULL, NULL),
    (NULL, "2,000,000 - 4,999,999","Y", NULL, NULL),
    (NULL, "5,000,000 - 20,000,000","Y", NULL, NULL),
    (NULL, "Lebih Dari 20,000,000","Y", NULL, NULL),
    (NULL, "Tidak Berpenghasilan","Y", NULL, NULL);


INSERT INTO `parent_jobs` VALUES
  (NULL, "Tidak Bekerja", "Y", NULL, NULL),
    (NULL, "Nelayan", "Y", NULL, NULL),
    (NULL, "Petani", "Y", NULL, NULL),
    (NULL, "Peternak", "Y", NULL, NULL),
    (NULL, "PNS/TNI/POLRI", "Y", NULL, NULL),
    (NULL, "Karyawan Swasta", "Y", NULL, NULL),
    (NULL, "Pedagang Kecil", "Y", NULL, NULL),
    (NULL, "Pedagang Besar", "Y", NULL, NULL),
    (NULL, "Wiraswasta", "Y", NULL, NULL),
    (NULL, "Wirausaha", "Y", NULL, NULL),
    (NULL, "Buruh", "Y", NULL, NULL),
    (NULL, "Pensiunan", "Y", NULL, NULL),
    (NULL, "Meninggal Dunia", "Y", NULL, NULL),
    (NULL, "Lain-lain", "Y", NULL, NULL);

